<?php require_once('../Connections/DBconfig.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
    session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
    $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['login'])) {
    $loginUsername = $_POST['login'];
    $password = $_POST['senha'];
    $MM_fldUserAuthorization = "nivel";
    $MM_redirectLoginSuccess = "../index.php";
    $MM_redirectLoginFailed = "index.php";
    $MM_redirecttoReferrer = false;
    mysql_select_db($database_DBconfig, $DBconfig);

    $LoginRS__query = sprintf("SELECT * FROM users WHERE userid=%s AND passwrd=%s AND statusUser = 1", GetSQLValueString($loginUsername, "text"), GetSQLValueString(md5($password), "text"));

    $LoginRS = mysql_query($LoginRS__query, $DBconfig) or die(mysql_error());
    $loginFoundUser = mysql_num_rows($LoginRS);
    if ($loginFoundUser) {

        $loginStrGroup = mysql_result($LoginRS, 0, 'nivel');
        $UserNome = mysql_result($LoginRS, 0, 'nomeUser');
        $UserId = mysql_result($LoginRS, 0, 'idUser');

        if (PHP_VERSION >= 5.1) {
            session_regenerate_id(true);
        } else {
            session_regenerate_id();
        }
        //declare two session variables and assign them
        $_SESSION['MM_UserId'] = $UserId;
        $_SESSION['MM_Username'] = $loginUsername;
        $_SESSION['MM_UserGroup'] = $loginStrGroup;
        $_SESSION['MM_UserNome'] = $UserNome;

        if (isset($_SESSION['PrevUrl']) && false) {
            $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];
        }
        header("Location: " . $MM_redirectLoginSuccess);
    } else {
        header("Location: " . $MM_redirectLoginFailed);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>BANCO DE CONHECIMENTO</title>
        <link href="../css/style.css" type="text/css" rel="stylesheet" />
    </head>

    <body>
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td height="55" class="colorbar">
                    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="50%"><!--<img src="http://www.secureoffice.com.br/eSIGA/logos/vertical-tradicional.bmp" width="100" height="105">--></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="5" class="colorbar2"></td>
            </tr>
            <tr>
                <td height="450px" class="maincontent" valign="top">
                    <table width="100%" border="0">
                        <tr>
                            <td width="36%" height="17">&nbsp;</td>
                            <td width="20%">&nbsp;</td>
                            <td width="44%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="170">&nbsp;</td>
                            <td><form id="form1" name="form1" method="POST" action="<?php echo $loginFormAction; ?>">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td colspan="2"><div align="center">Login de Acesso</div></td>
                                        </tr>
                                        <tr>
                                            <td width="43%"><div align="right">Login:</div></td>
                                            <td width="57%"><label for="login"></label>
                                                <input type="text" name="login" id="login" /></td>
                                        </tr>
                                        <tr>
                                            <td><div align="right">Senha:</div></td>
                                            <td><label for="senha"></label>
                                                <input type="password" name="senha" id="senha" /></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><input type="submit" name="logar" id="logar" value="Logar" /></td>
                                        </tr>
                                    </table>
                                </form></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="200">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <!-- End Main Mody -->
                </td>
            </tr>
            <tr>
                <td height="1" class="hrz_line"></td>
            </tr>
            <tr>
                <td height="50" align="right" class="foot">Desenvolvido por <a href="http://www.sigati.com.br" target="_blank">SIGA TI Tecnologia da Informa&ccedil;&atilde;o</a>. Copyright &copy; 2013 - <?php echo date('Y') ?>.</td>
            </tr>
        </table>
    </body>
</html>

