<?php

if (!isset($_SESSION)) {
    session_start();
}
if (date('H') < 12):
    echo 'Bom dia <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
elseif (date('H') < 19):
    echo 'Boa tarde <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
else:
    echo 'Boa Noite <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
endif;
?>