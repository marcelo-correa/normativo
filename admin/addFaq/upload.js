$(document).ready(function() {

    function Upload() {
        $('#formUpload').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {
                $('progress').attr('value', percentComplete);
                $('#porcentagem').html(percentComplete + '%');
            },
            success: function(data) {
                $('progress').attr('value', '100');
                $('#porcentagem').html('100%');
                if (data.sucesso === true) {
                    $(".NomeArquivo").val(data.NomeArquivo);
                    $(".NovaResposta").val('Clique aqui para ver a Resposta!');
                    $('.resposta').html('Arquivo enviado com Sucesso!');
                    //$('#resposta').html('<img src="' + data.msg + '" />');

                    SalvarFaq();

                }
                else {
                    $('#resposta').html(data.msg);
                }
            },
            error: function() {
                $('#resposta').html('Erro ao enviar requisi��o!!!');
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: true
        }).submit();
    }

    function SalvarFaq() {
        var SupCategoriaId = $(".SupCategoriaId").val();
        var CategoriaId = $("#CategoriaId").val();
        var Pergunta = $("#NovaPergunta").val();
        var Resposta = $(".NovaResposta").val();
        var Link = $(".NomeArquivo").val();

        if (CategoriaId === "") {
            alert("Informe o ID da Categoria");
        } else if (Pergunta === "") {
            alert("Informe a Pergunta");
        } else if (Resposta === "") {
            alert("Informe a Resposta ou Selecione um Arquivo");
        } else if (Resposta === "Clique aqui para ver a Resposta!") {

            if (Link === "") {
                alert("Selecione um Arquivo");
            } else {
                AddFaq(SupCategoriaId, CategoriaId, Pergunta, Resposta, Link);
            }
        } else {
            AddFaq(SupCategoriaId, CategoriaId, Pergunta, Resposta, Link);
        }

        function AddFaq(SupCategoriaId, CategoriaId, Pergunta, Resposta, Link) {

            $.post("../php/php.php", {
                SupCategoriaId: SupCategoriaId,
                CategoriaId: CategoriaId,
                Pergunta: Pergunta,
                Resposta: Resposta,
                Link: Link,
                acao: 'AddFaq'
            }, function(eAddFaq) {
                if (eAddFaq) {
                    //window.setTimeout('location.reload()');

                    alert('Faq Cadastrado com Sucesso');

                    window.setTimeout('close()');


                }
            });
        }
    }

    $(".AdicionarFaq").click(function() {

        var arquivo = $('.arquivo').val();

        if (arquivo !== "") {

            $.post("verificar_arquivo.php", {
                arquivo: arquivo
            }, function(eArquivo) {

                if (eArquivo === '1') {

                    Upload();

                } else {

                    $('.resposta').html(' A Exten��o ' + eArquivo + ' N�o Permitida.');

                }
            });
        } else {

            SalvarFaq();

        }




    });

    $('.fechar').click(function() {
        var CategoriaId = $("#CategoriaId").val();
        var Link = $(".NomeArquivo").val();
        if (Link !== "") {

            $.post("../php/php.php", {
                CategoriaId: CategoriaId,
                Link: Link,
                acao: "DelArquivo"
            }, function(eDelArquivo) {

                if (eDelArquivo) {

                    window.setTimeout('close()');
                }
            });
        } else {
            window.setTimeout('close()');
        }
    });


});