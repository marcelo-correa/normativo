<div id="dialog-form" title="" style="display: none; font-size: 80%;">
    <p class="validateTips">Clique na pergunta para revelar a resposta.</p>
    <table width="100%" border="0"></table>
</div>
<div id="dialog-form-pdf" title="" style="display: none; font-size: 80%;">
    <iframe name="conteudo" id="conteudo" width="100%" height="99%" frameborder="0"></iframe>
</div>
<div id="dialog-form-alterarSenha" class="AddAlt" title="Alterar Senha" style="display: none; font-size: 80%;">
    <label>
        <span>Senha Atual</span>
        <input type="password" class="SenhaAtual">
    </label>
    <label>
        <span>Nova Senha</span>
        <input type="password" class="NovaSenha">
    </label>
    <label>
        <span>Confirme Senha</span>
        <input type="password" class="ConfirmSenha">
    </label>
</div>

<div id="dialog-form-Searh" class="AddAlt" title="" style="display: none; font-size: 80%;">
    <table width="100%" border="0"></table>
</div>

<div id="dialog-form-adicionarFaq" class="AddAlt" title="Adicionar Faq" style="display:none; font-size: 80%;">
    <fieldset>
        <form name="adicionarFaq" action="" method="post">
            <input type="hidden" id="CategoriaId" class="CategoriaId" value="1"/>
            <input type="hidden" id="SupCategoriaId" class="SupCategoriaId" value="1" />
            <label>
                <span>Pergunta:</span>
                <textarea id="NovaPergunta" class="NovaPergunta"></textarea>
            </label>
            <label>
                <span>Resposta:</span>
                <textarea id="Resposta" class="NovaResposta"></textarea>
            </label>
            <label>
                <span>Nome do Arquivo:</span>
                <input type="text" id="NomeArquivo" class="NomeArquivo" readonly="" name="NomeArquivo">
            </label>
        </form>
        <?php include './formUpload.php'; ?>
    </fieldset>
</div>