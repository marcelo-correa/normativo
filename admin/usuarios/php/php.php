<?php

//FUN��O PARA RESOLVER PROBLEMAS DE ACENTUA��O
header('Content-Type: text/html; charset=iso 8859-9');
//INICIA SESS�O
session_start();
//CHAMA CONEC��O
include '../../../Connections/DB.class.php';
require '../../../classes/carregaClassAdmin3.php';

//INSTACIA A CLASSE
$Functions = new functions();

//SWITCH PARA RECEBER OS DADOS QUE VEM VIA $.post DO JQUERY
switch ($_POST['acao']) {
    case"MarcarComoEnviado":

        $Functions->setUser_id($_POST['getIdUser']);
        $Functions->UpdateEmailEnviado();

        break;
    //DELETAR USUARIO
    case"DelUsuario":
        //RECEBE O ID DO USUARIO
        $Functions->setIdUser($_POST['UsuarioId']);
        //CHAMA A FUN��O
        if ($Functions->DelUsuario()):
            //SE DELETADO DELETAR TAMBEM AS PERMISS�ES PARA ESTE USUARIO
            //RECEBE O ID DO USUARIO
            $Functions->setUser_id($_POST['UsuarioId']);
            //CHAMA A FUN��O
            $Functions->DelPermissionByUsuario();
            //CHAMA A FUN��O PARA LISTAR TODOS OS USU�RIOS
            $Functions->ListarUsuarios();
            echo '<table width="50%" border="0" id="table">
                    <tr>
                       <td style="width:10%">C&oacute;digo</td>
                       <td style="width:40%">Nome</td>
                       <td style="width:5%">Nivel</td>
                       <td style="width:5%">&nbsp;</td>
                       <td style="width:8%">&nbsp;</td>
                    </tr>
                   <div id="catResult">';
            //RETORNA O ARRAY
            foreach ($Functions->ListarUsuarios() as $Users) {
                //TRATA O RESULTADO PARA O NOVEL DE USUARIO
                if ($Users['nivel'] == 0):
                    $Nivel = 'Usu�rio';
                elseif ($Users['nivel'] == 1):
                    $Nivel = 'Administrador';
                endif;
                echo '<tr>
                         <td>' . $Users['idUser'] . '</td>
                         <td>' . $Users['nomeUser'] . '</td>
                         <td>' . $Nivel . '</td>
                         <td><a href="visualizar.php?idUser=' . $Users['idUser'] . '&name=' . $Users['nomeUser'] . '">Permiss�es</a></td>
                         <td><a href="javascript:void(0);" class="visualiz">Editar</a></td>
                         <td><a href="javascript:void(0);" class="deletarUsuario">Deletar</a></td>
                      </tr>';
            }
            echo '</div><!-- catResult -->
                </table>';
        endif;
        break;
    //SELECIONAR USUARIO PARA EDITAR
    case"SelecionarUsuario":
        //RECEBE O ID DO USU�RIO
        $Functions->setIdUser($_POST['UsuarioId']);
        //CHAMA A FUN��O
        $Functions->SelecionarUsuario();
        //RETORNA O ARRAY
        foreach ($Functions->SelecionarUsuario() as $dados) {
            if ($dados["idUser"] == 1):
                $Disabled = 'disabled';
            else:
                $Disabled = '';
            endif;



            echo '<input type="hidden" class="IdUser" value="' . $dados["idUser"] . '">
                    <label>
                        <span>Login:</span>
                        <input type="text" class="LoginUser" disabled="" value="' . $dados["userid"] . '">
                    </label>
                    <label>
                        <span>Nome:</span>
                        <input type="text" class="NomeUser" value="' . $dados["nomeUser"] . '">
                    </label>
                    <label>
                    <span>Status:</span>
                    <select class="StatusUser" ' . $Disabled . '>';
            //TRATAMENTO PARA STATUS
            if ($dados["statusUser"] == 1):
                echo '<option value="1" >Ativo</option>
                          <option value="0">Desativar</option>';
            else:
                echo '<option value="0">Desativado</option>
                          <option value="1">Ativar</option>';
            endif;
            echo'</select>
                    </label>
                    <label>
                        <span>N�vel:</span>
                        <select class="NivelUser" ' . $Disabled . '>';
            //TRATAMENTO PARA NIVEL
            if ($dados["nivel"] == 1):
                echo '<option value="1">Administrador</option>
                      <option value="0">Usu�rio</option>';
            else:
                echo '<option value="0">Usu�rio</option>
                      <option value="1">Administrador</option>';
            endif;
            echo'</select>
                    </label>
                    <label>
                        <span>Email:</span>
                        <input type="text" class="EmailUser" value="' . $dados["email"] . '">
                    </label>';
        }
        break;
    //ADICIONAR USUARIO
    case"AddUsuario":
        //RECEBE O LOGIN PARA O USU�RIO
        $Functions->setUserid(utf8_decode($_POST['LoginUser']));
        //RECEBE A SENHA E TRATAMENTO COM MD5
        $Functions->setPasswrd(md5($_POST['SenhaUser']));
        //RECEBE O NOME DO USUARIO
        $Functions->setNomeUser(utf8_decode($_POST['NomeUser']));
        //RECEBE O NIVEL DO USUARIO
        $Functions->setNivel($_POST['NivelUser']);
        //CHAMA A FUN��O
        $Functions->setEmail($_POST['EmailUser']);

        $CountUsers = count($Functions->duplicidadeUsuario());

        if ($CountUsers <= 0):
            if ($Functions->addUsuario()):
                echo 1;
            endif;
        else:
            echo 0;
        endif;

        break;
    //EDITAR USUARIO
    case"EditUsuario":
        //RECEBE O NOVO LOGIN
        $Functions->setUserid(utf8_decode($_POST['LoginUser']));
        //RECEBE O NOVO NOME
        $Functions->setNomeUser(utf8_decode($_POST['NomeUser']));
        //RECEBE O NOVO NIVEL DE USUARIO
        $Functions->setNivel($_POST['NivelUser']);
        //RECEBE O STATUS
        $Functions->setStatusUser($_POST['StatusUser']);

        $Functions->setEmail($_POST['EmailUser']);
        //RECEBE O ID DO USUARIO
        $Functions->setIdUser($_POST['IdUser']);
        //CHAMA A FUN��O PARA EDITAR
        if ($Functions->EditUsuario()):
            //SE TUDO OK
            //LISTA TODOS OS USUARIOS
            $Functions->ListarUsuarios();
            echo '<table width="50%" border="0" id="table">
                    <tr>
                       <td style="width:10%">C&oacute;digo</td>
                       <td style="width:40%">Nome</td>
                       <td style="width:5%">Nivel</td>
                       <td style="width:5%">Status</td>
                       <td style="width:5%">Permiss�es</td>
                       <td style="width:5%">&nbsp;</td>
                       <td style="width:8%">&nbsp;</td>
                    </tr>
                    <div id="catResult">';
            //RECEBE O ARRAY
            foreach ($Functions->ListarUsuarios() as $Users) {
                //TRATAMENTO DO STATUS
                if ($Users['statusUser'] == 1):
                    $Status = 'Ativado';
                else:
                    $Status = 'Desativado';
                endif;
                //TRATAMENTO DO NIVEL
                if ($Users['nivel'] == 0):
                    $Nivel = 'Usu�rio';
                elseif ($Users['nivel'] == 1):
                    $Nivel = 'Administrador';
                endif;

                echo '<tr>
                        <td>' . $Users['idUser'] . '</td>
                        <td>' . $Users['nomeUser'] . '</td>
                        <td>' . $Status . '</td>
                        <td>' . $Nivel . '</td>
                        <td><a href="visualizar.php?idUser=' . $Users['idUser'] . '&name=' . $Users['nomeUser'] . '">Permiss�es</a></td>
                        <td><a href="javascript:void(0);" class="visualiz">Editar</a></td>
                        <td><a href="javascript:void(0);" class="deletarUsuario">Deletar</a></td>
                    </tr>';
            }
            echo '</div><!-- catResult -->
	</table>';
        endif;
        break;
    //ADIONAR PERMISS�O
    case"AddPermissao":
        //RECEBE O ID DO FAQ
        $Functions->setFaq_id($_POST['faqId']);
        $Functions->ListarIdFaq();
        foreach ($Functions->ListarIdFaq() as $dados):

            $Functions->setCategoria_id($dados['category_id']);
            $Functions->setSupCat_id($dados['supCategoria_id']);
            $Functions->setFaqs_id($_POST['faqId']);
            $Functions->setUser_id($_POST['idUser']);
            //VERIFICA��O DE DUPLICIDADE
            $CountPermission = count($Functions->ListarPermission());
            //SE N�O TIVER A PERMISS�O
            if ($CountPermission == 0):
                //CHAMA A FUN��O
                $Functions->AddPermission();
            endif;

        endforeach;
        break;
    case"AddPermissaoByCategoria":

        $Functions->setCategory_id($_POST['CategoriaId']);
        $Functions->ListarFaqsByCategoria();

        foreach ($Functions->ListarFaqsByCategoria() as $dados):
            $Functions->setCategoria_id($_POST['CategoriaId']);
            $Functions->setSupCat_id($_POST['SuperCategoriaId']);
            $Functions->setFaqs_id($dados['id']);
            $Functions->setUser_id($_POST['idUser']);
            $Functions->AddPermission();
        endforeach;

        break;
    case"AddPermissaoBySupCategoria":

        $Functions->setSupCategoria_id($_POST['SuperCategoriaId']);
        $Functions->ListarFaqsBySupCategoria();
        foreach ($Functions->ListarFaqsBySupCategoria() as $dados):

            $Functions->setCategoria_id($dados['category_id']);
            $Functions->setSupCat_id($dados['supCategoria_id']);
            $Functions->setFaqs_id($dados['id']);
            $Functions->setUser_id($_POST['UsuarioId']);
            $Functions->AddPermission();

        endforeach;

        break;
    //DELETAR UMA PERMISS�O
    case"DelPermissao":
        //RECEBE O ID DO USUARIO
        $Functions->setUser_id($_POST['idUser']);
        //RECEBE O ID DO FAQ
        $Functions->setFaqs_id($_POST['faqId']);
        //ARMAZENA O MUMERO DE LINHA NA VARIAVEL
        $CountPermission = count($Functions->ListarPermission());
        //RECEBE O ARRAY
        foreach ($Functions->ListarPermission() as $dadosPermission) {

            if ($CountPermission >= 1):
                echo $dadosPermission['id'];
                $Functions->setPermiss_id($dadosPermission['id']);
                $Functions->DelPermission();
            endif;
        }
        break;
    case"DelPermissaoByCategoria":

        $Functions->setUser_id($_POST['idUser']);

        $Functions->setCategoria_id($_POST['CategoriaId']);

        $Functions->DelPermissionByUsuarioAndCategory();
        break;
    case"DelPermissaoBySupCategoria":

        $Functions->setUser_id($_POST['UsuarioId']);

        $Functions->setSupCat_id($_POST['SuperCategoriaId']);

        $Functions->DelPermissionByUsuarioAndSupCategory();
        break;
    case"BuscarDadosEmail":

        $Functions->setUser_id($_POST['getIdUser']);
        $CountPermissaoSemEmail = count($Functions->ListarPermissaoSemEmail());

        if ($CountPermissaoSemEmail >= 1):

            echo '<table width="100%" border="0">
                <tr>
                  <td width="9%"><img src="http://www.secureoffice.com.br/eSIGA/logos/vertical-tradicional.bmp" width="100" height="105"></td>
                  <td width="13%">&nbsp;</td>
                  <td width="32%">&nbsp;</td>
                  <td width="46%">&nbsp;</td>
                </tr>
                <tr>';
            $Functions->setIdUser($_POST['getIdUser']);
            $Functions->SelecionarUsuario();
            foreach ($Functions->SelecionarUsuario() as $user):
            endforeach;
            echo'  <td colspan="3">Ol� Usu�rio <strong style="font-weight: bold;">' . $user['nomeUser'] . '</strong>, voc� recebeu acesso aos seguintes procedimentos:</td>
                  <td>&nbsp;</td>
                </tr>';
            $Functions->setUser_id($_POST['getIdUser']);
            $Functions->ListarPermissaoSemEmail();
            foreach ($Functions->ListarPermissaoSemEmail() as $dados):
                echo'<tr>
                  <td>&nbsp;</td>
                  <td colspan="3">' . $dados['question'] . '</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="3">' . $dados['answer'] . '</td>
                  </tr>';
            endforeach;
            echo'<tr>
                <td colspan="4">Atenciosamente,</td>
              </tr>
              <tr>
                <td colspan="4">sos@sosinformatica.inf.br</td>
              </tr>
            </table>';

        else:

            echo $CountPermissaoSemEmail;

        endif;

        break;
    case"EnviarEmail":
        require '../mailer/config.php';

        $Functions->setIdUser($_POST['getIdUser']);
        $Functions->SelecionarUsuario();
        foreach ($Functions->SelecionarUsuario() as $dados):
        //echo $dados['email'];
        endforeach;
        $msg = utf8_decode($_POST['msg']);
        if (sendMail('BANCO DE CONHECIMENTO SIGA TI', $msg, 'sos@sosinformatica.inf.br', $dados['email'])):

            $Functions->setUser_id($_POST['getIdUser']);
            $Functions->UpdateEmailEnviado();

            echo 1;
        endif;
        break;
}
?>
