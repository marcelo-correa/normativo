$(document).ready(function() {
    $('.btnEnviar').click(function() {

        var arquivo = $('.arquivo').val();

        $.post("uploads/verificar_arquivo.php", {
            arquivo: arquivo
        }, function(eArquivo) {
            if (eArquivo === '1') {
                $('.formUpload').ajaxForm({
                    uploadProgress: function(event, position, total, percentComplete) {
                        $('progress').attr('value', percentComplete);
                        $('.porcentagem').html(percentComplete + '%');
                    },
                    success: function(data) {
                        $('progress').attr('value', '100');
                        $('.porcentagem').html('100%');
                        if (data.sucesso === true) {
                            $(".NomeArquivo").val(data.NomeArquivo);
                            $(".NovaResposta").val('Clique aqui para ver a Resposta!');
                            $('.resposta').html('Arquivo enviado com Sucesso!');
                        }
                        else {
                            $('.resposta').html(data.msg);
                        }
                    },
                    error: function() {
                        $('.resposta').html('Erro ao enviar requisi��o!!!');
                    },
                    dataType: 'json',
                    url: 'uploads/enviar_arquivo.php',
                    resetForm: false
                }).submit();

            } else {

                $('.resposta').html(' A Exten��o ' + eArquivo + ' N�o Permitida.');

            }
        });
    });
});