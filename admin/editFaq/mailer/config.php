<?php

function sendMail($assunto, $msg, $nomeRemetente, $destino) {
    require_once ('../editFaq/mailer/class.phpmailer.php');
    $mail = new PHPMailer();

    $mail->IsSMTP();
    //$mail->SMTPDebug = true;
    $mail->SMTPAuth = true;                  // enable SMTP authentication
    $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
    $mail->Host = "mail.sosinformatica.inf.br";      // sets GMAIL as the SMTP server
    $mail->Port = 465;                   // set the SMTP port for the GMAIL server
    $mail->Username = "sos.caixa@sosinformatica.inf.br";  // GMAIL username
    $mail->Password = "sosp10";
    $mail->charSet = "UTF-8";
    //$mail->ConfirmReadingTo = "sos.caixa@sosinformatica.inf.br";
//$mail->From = $remetente;
    $mail->FromName = $nomeRemetente;

    $mail->IsHTML(true);

    $mail->Subject = $assunto;
    $mail->Body = $msg;
    $mail->AddAddress($destino);


    if (!$mail->Send()) {
        return false;
    } else {

        return true;
    }
}

?>
