<?php
if ( !isset( $_SESSION ) ) {
    session_start();
}
include_once("header.php");
include 'Connections/DB.class.php';
require './classes/carregaClass.php';
?>


<div id="box">
    <a href="deslogar.php">Deslogar</a> / <a href="javascript:void(0);" class="alterarSenha">Alterar Senha</a> /
    <?php
    if ( $_SESSION['MM_UserGroup'] == '1' ):
        echo '<a href="admin/index.php">Administração</a><br>';
    else:
        echo '<a href="javascript:void(0);" class="AdicionarFaq">Adicionar Faq</a>';
    endif;
    ?>
    <br>
    <form name="Localizar" id="Localizar" class="Localizar" action="" method="POST">
        <label>
            <input type="text" name="Search" id="Search" class="Search" style="padding:5px; width:200px;">
        </label>
        <input type="button" name="SearchBtn" id="SearchBtn" class="SearchBtn AddAlt" style="padding:5px; width:100px;" value="Localizar">
    </form>
    <?php
    echo'<br>';
    include './saudacoes.php';

    echo '<p>Selecione uma categoria para ver as Perguntas Mais Frequentes associadas.</p>';
    ?>
</div>
<div id="ListaFaqs">
    <ul>
        <?php
        $Functions = new functions();

        $Functions->setUser_id( $_SESSION['MM_UserId'] );
        $Functions->ListarSuperCategoriasPermitidasGroupBySupCatId();
        foreach ( $Functions->ListarSuperCategoriasPermitidasGroupBySupCatId() as $dadosSupCategoria ) {
            echo '<li><span style="display: none;"> ' . $dadosSupCategoria['supCatId'] . ' </span><a href="javascript:void(0);" class="liSuper">' . $dadosSupCategoria['supCatNome'] . '</a>';
            echo '<ul>';

            $Functions->setUser_id( $_SESSION['MM_UserId'] );
            $Functions->setSCatId( $dadosSupCategoria['supCatId'] );
            $Functions->ListarCategoriasPermitidasGroupByCategory();
            foreach ( $Functions->ListarCategoriasPermitidasGroupByCategory() as $dadosCategoria ) {
                echo '<li class="liCategoria" style="display: none;">
                            <span style="display: none;"> ' . $dadosCategoria['category_id'] . ' </span>
                            <a href="javascript:void(0);" class="visualiz" title="' . $dadosCategoria['category'] . '" alt="' . $dadosCategoria['category_id'] . '">' . $dadosCategoria['category'] . '</a>
                      </li>';
            }

            echo '</ul>';
            echo '</li>';
        }
        ?>
    </ul>
</div>

<?php include("./dialog.php"); ?>
<?php include_once("footer.php"); ?>