<?php

class getSet {

    //Users
    private $idUser;
    private $userid;
    private $passwrd;
    private $nomeUser;
    private $nivel;
    private $grupoUser;
    private $statusUser;
    private $email;
    //FaqsCategorias
    private $faqsCat_id;
    private $category;
    private $SCatId;
    //SUPER CATEGORIA
    private $supCatId;
    private $supCatNome;
    //Faqs
    private $faq_id;
    private $category_id;
    private $supCategoria_id;
    private $question;
    private $answer;
    private $link;
    private $linkExterno;
    private $dataCadastro;
    private $dataModificado;
    private $userInclusao;
    private $userModificado;
    //Permiss�o
    private $permiss_id;
    private $faqs_id;
    private $status;
    private $user_id;
    private $grupoPermissionUser;
    private $email_enviado;
    private $categoria_id;
    private $SupCat_id;

    public function getIdUser() {
        return $this->idUser;
    }

    public function setIdUser($idUser) {
        $this->idUser = $idUser;
    }

    public function getUserid() {
        return $this->userid;
    }

    public function setUserid($userid) {
        $this->userid = $userid;
    }

    public function getPasswrd() {
        return $this->passwrd;
    }

    public function setPasswrd($passwrd) {
        $this->passwrd = $passwrd;
    }

    public function getNomeUser() {
        return $this->nomeUser;
    }

    public function setNomeUser($nomeUser) {
        $this->nomeUser = $nomeUser;
    }

    public function getNivel() {
        return $this->nivel;
    }

    public function setNivel($nivel) {
        $this->nivel = $nivel;
    }

    public function getGrupoUser() {
        return $this->grupoUser;
    }

    public function setGrupoUser($grupoUser) {
        $this->grupoUser = $grupoUser;
    }

    public function getStatusUser() {
        return $this->statusUser;
    }

    public function setStatusUser($statusUser) {
        $this->statusUser = $statusUser;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getFaqsCat_id() {
        return $this->faqsCat_id;
    }

    public function setFaqsCat_id($faqsCat_id) {
        $this->faqsCat_id = $faqsCat_id;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->category = $category;
    }

    public function getSCatId() {
        return $this->SCatId;
    }

    public function setSCatId($SCatId) {
        $this->SCatId = $SCatId;
    }

    public function getSupCatId() {
        return $this->supCatId;
    }

    public function setSupCatId($supCatId) {
        $this->supCatId = $supCatId;
    }

    public function getSupCatNome() {
        return $this->supCatNome;
    }

    public function setSupCatNome($supCatNome) {
        $this->supCatNome = $supCatNome;
    }

    public function getFaq_id() {
        return $this->faq_id;
    }

    public function setFaq_id($faq_id) {
        $this->faq_id = $faq_id;
    }

    public function getCategory_id() {
        return $this->category_id;
    }

    public function setCategory_id($category_id) {
        $this->category_id = $category_id;
    }

    public function getSupCategoria_id() {
        return $this->supCategoria_id;
    }

    public function setSupCategoria_id($supCategoria_id) {
        $this->supCategoria_id = $supCategoria_id;
    }

    public function getQuestion() {
        return $this->question;
    }

    public function setQuestion($question) {
        $this->question = $question;
    }

    public function getAnswer() {
        return $this->answer;
    }

    public function setAnswer($answer) {
        $this->answer = $answer;
    }

    public function getLink() {
        return $this->link;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function getLinkExterno() {
        return $this->linkExterno;
    }

    public function setLinkExterno($linkExterno) {
        $this->linkExterno = $linkExterno;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    public function getDataModificado() {
        return $this->dataModificado;
    }

    public function setDataModificado($dataModificado) {
        $this->dataModificado = $dataModificado;
    }

    public function getUserInclusao() {
        return $this->userInclusao;
    }

    public function setUserInclusao($userInclusao) {
        $this->userInclusao = $userInclusao;
    }

    public function getUserModificado() {
        return $this->userModificado;
    }

    public function setUserModificado($userModificado) {
        $this->userModificado = $userModificado;
    }

    public function getPermiss_id() {
        return $this->permiss_id;
    }

    public function setPermiss_id($permiss_id) {
        $this->permiss_id = $permiss_id;
    }

    public function getFaqs_id() {
        return $this->faqs_id;
    }

    public function setFaqs_id($faqs_id) {
        $this->faqs_id = $faqs_id;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    public function getGrupoPermissionUser() {
        return $this->grupoPermissionUser;
    }

    public function setGrupoPermissionUser($grupoPermissionUser) {
        $this->grupoPermissionUser = $grupoPermissionUser;
    }

    public function getEmail_enviado() {
        return $this->email_enviado;
    }

    public function setEmail_enviado($email_enviado) {
        $this->email_enviado = $email_enviado;
    }

    public function getCategoria_id() {
        return $this->categoria_id;
    }

    public function setCategoria_id($categoria_id) {
        $this->categoria_id = $categoria_id;
    }

    public function getSupCat_id() {
        return $this->SupCat_id;
    }

    public function setSupCat_id($SupCat_id) {
        $this->SupCat_id = $SupCat_id;
    }

}

?>
