$(document).ready(function() {


//    $.ajax({
//        type: "POST",
//        url: "",
//        async: false,
//        data: {
//        },
//        success: function() {
//
//        }
//    });

    function UploadEdit() {

        $('#formUpload').ajaxForm({
            uploadProgress: function(event, position, total, percentComplete) {
                $('progress').attr('value', percentComplete);
                $('#porcentagem').html(percentComplete + '%');
            },
            success: function(data) {
                $('progress').attr('value', '100');
                $('#porcentagem').html('100%');
                if (data.sucesso === true) {
                    $(".NomeArquivo").val(data.NomeArquivo);
                    $(".Resposta").val('Clique aqui para ver a Resposta!');
                    $('.resposta').html('Arquivo enviado com Sucesso!');
                    //$('#resposta').html('<img src="' + data.msg + '" />');

                    EditarFaq();

                }
                else {
                    $('#resposta').html(data.msg);
                }
            },
            error: function() {
                $('#resposta').html('Erro ao enviar requisi��o!!!');
            },
            dataType: 'json',
            url: 'enviar_arquivo.php',
            resetForm: true
        }).submit();
    }

    function EditarFaq() {
        var CategoriaFixo = $("#Fixo").val();
        var Categoria = $("#CatFaq").val();
        var NomeArquivo = $("#Arquivo").val();


        if (CategoriaFixo !== Categoria && NomeArquivo !== "") {
            $.ajax({
                type: "POST",
                url: "../php/php.php",
                async: false,
                data: {
                    Categoria: Categoria,
                    CategoriaFixo: CategoriaFixo,
                    NomeArquivo: NomeArquivo,
                    acao: "MoverArquivo"
                },
                success: function(eMoverArquivo) {
                    if (eMoverArquivo === '1') {
                        EditFaq();
                    }
                }
            });

        } else {

            EditFaq();
        }
    }

    function EditFaq() {

        var IdFaq = $(".F_Id").val();
        var Categoria = $("#CatFaq").val();
        var SupCatFaq = $('.SupCatFaq').val();
        var Pergunta = $(".Pergunta").val();
        var Resposta = $("#RespostaFaq").val();
        var NomeArquivo = $("#Arquivo").val();
        var ArquivoAntigo = $(".arquivo_antigo").val();
        var CategoriaFixo = $("#Fixo").val();

        if (Categoria === null) {

            alert('Selecione uma Categoria');
        } else if (Pergunta === "") {

            alert('Informe a Pergunta');
        } else if (Resposta === "") {

            alert('Informe a Resposta');
        } else {

            $.ajax({
                type: "POST",
                url: "../php/php.php",
                async: false,
                data: {
                    IdFaq: IdFaq,
                    SupCatFaq: SupCatFaq,
                    Categoria: Categoria,
                    Pergunta: Pergunta,
                    Resposta: Resposta,
                    NomeArquivo: NomeArquivo,
                    CategoriaFixo: CategoriaFixo,
                    ArquivoAntigo: ArquivoAntigo,
                    acao: "AlterarFaq"
                },
                success: function(retAlterarFaq) {
                    if (retAlterarFaq) {

                        $('.btn').find('.enviar_email').each(function() {

                            if (this.checked === true) {

                                $.ajax({
                                    type: "POST",
                                    url: "../php/php.php",
                                    async: false,
                                    data: {
                                        IdFaq: IdFaq,
                                        acao: "EnviarEmail"
                                    },
                                    success: function(eEnviarEmail) {
                                        if (eEnviarEmail) {
                                            alert('Email enviado com Sucesso!');
                                        }
                                    }
                                });
                            }
                        });

                        //window.setTimeout(1000);

                        alert('Faq Alterado com Sucesso!');

                        window.setTimeout('close()');
                    }
                }
            });
        }
    }

    $("#EditarFaqedit").click(function() {

        var arquivo = $('.arquivo').val();

        if (arquivo !== "") {

            $.post("verificar_arquivo.php", {
                arquivo: arquivo
            }, function(eArquivo) {

                if (eArquivo === '1') {

                    UploadEdit();

                } else {

                    $('.resposta').html(' A Exten��o ' + eArquivo + ' N�o Permitida.');

                }
            });
        } else {

            EditarFaq();

        }

    });

    $('.fechar').click(function() {

        window.setTimeout('close()');

    });

    $('body').delegate(".SupCatFaq", "change", function() {

        var SupCategoriaId = $(this).val();

        $.post("../php/php.php", {
            SupCategoriaId: SupCategoriaId,
            acao: "ListarCategoriaBySupCategoria"
        }, function(eListarCategoriaBySupCategoria) {

            $('.CatFaq').html(eListarCategoriaBySupCategoria);

        });
    });


});