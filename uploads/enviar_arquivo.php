<?php

//IMPORTA O ARQUIVO ONDE A FUN��O ULPOAD EST� IMPLEMETADA
require_once('funcao_upload.php');

//RECEBE O ARQUIVO SELECIONADO
$arquivo = $_FILES['arquivo'];
//RECEBE O ID DA CATEGORIA
$categoria = $_POST['CatUpload'];
//DEFINE OS TIPOS DE ARQUIVOS PERMITIDOS
$tipos = array('pdf');
//DEFINE O LUGAR PARA UPLOAD
$pasta = '../admin/uploads/uploads/' . $categoria . '/';
//CHAMA A FUN��O PARA ENVIAR O ARQUIVO E ARMAZENA O RESULTADO NA VARIAVEL $enviar
$enviar = uploadFile($arquivo, $pasta, $tipos);
//INSTANCIA A VARIAVEL
$data['sucesso'] = false;
//VERIFICA O RESULTADO FOI ERRO
if ($enviar['erro']) {
    //ARMAZENA NA VARIALVEL $data O TIPO DE ERRO
    $data['msg'] = $enviar['erro'];
} else {
    //SE TUDO OK
    //MOSTRA A VARIAVEL $data PARA TRUE PARA SER TRATADA PELO JQUERY
    $data['sucesso'] = true;
    //INFORMA O CAMINHO DO ARQUIVO
    $data['msg'] = $enviar['caminho'];
    //INFORMA O NOME DO ARQUIVO
    $data['NomeArquivo'] = $enviar['caminho'];
}
//CODIFICA A VARIALVEL PARA JSON
echo json_encode($data);