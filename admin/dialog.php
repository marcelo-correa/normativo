<?php
include_once '../Connections/DB.class.php';
//require '../classes/carregaClassAdmin.php';
$Functions = new functions();
?>
<div id="dialog-form-Searh" class="AddAlt" title="" style="display: none; font-size: 80%;">
    <table width="100%" border="0"></table>
</div>
<div id="dialog-form-visualizarFaqs" style="display: none; font-size: 80%;">

    <p class="validateTips">Clique na pergunta para revelar a resposta.</p>

    <table width="100%" border="0"></table>

</div>

<div id="dialog-form-adicionarCategoria" class="AddAlt" title="Adicionar Categoria" style="display:none; font-size: 80%;">

    <form name="alterarFaq" action="" method="post">
        <label>
            <span>Sup. Categoria</span>
            <select name="SelSupCategoria" class="SelSupCategoria" id="SelSupCategoria">
                <option value="">Selecione...</option>
                <?php
                $Functions->ListarSuperCategoria();
                foreach ($Functions->ListarSuperCategoria() as $dados) {
                    echo '<option value="' . $dados['supCatId'] . '">' . $dados['supCatNome'] . '</option>';
                }
                ?>

            </select>
        </label>
        <label>
            <span>Descri&ccedil;&atilde;o:</span>
            <input type="text" id="NewCategoria" />
        </label>
    </form>
</div>

<div id="dialog-form-adicionarSupCategoria" class="AddAlt" title="Adicionar Super Categoria" style="display:none; font-size: 80%;">

    <form name="" action="" method="post">
        <label>
            <span>Descri&ccedil;&atilde;o:</span>
            <input type="text" id="NewSupCategoria" />
        </label>
    </form>
</div>

<div id="dialog-form-editSupCategoria" class="AddAlt" title="Editar Super Categoria" style="display:none; font-size: 80%;">

    <form name="" action="" method="post">
        <label>
            <span>Descri&ccedil;&atilde;o:</span>
            <input type="text" id="editSupCategoria" />
        </label>
        <input type="hidden" id="idEditSupCategoria" />
    </form>
</div>

<div id="dialog-form-editarCategoria" class="AddAlt" style="display:none; font-size: 80%;">

    <form name="editCategoria" action="" method="post">
        <label>
            <span>Descri&ccedil;&atilde;o:</span>
            <input type="text" id="EditCategoria" />
            <input type="hidden" id="IdCategoria"/>
        </label>
    </form>
</div>