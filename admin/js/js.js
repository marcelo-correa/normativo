$(document).ready(function() {

    $(".liSuper").click(function() {

        $(".liSuper").parent().children().find('li').slideUp();

        $(this).parent().children().find('li').slideToggle();

    });

    $('body').delegate(".SupCatFaq", "change", function() {

        var SupCategoriaId = $(this).val();

        $.post("php/php.php", {
            SupCategoriaId: SupCategoriaId,
            acao: "ListarCategoriaBySupCategoria"
        }, function(eListarCategoriaBySupCategoria) {

            $('.CatFaq').html(eListarCategoriaBySupCategoria);

        });
    });

    function impedeLetras(obj) {
        $(obj).keypress(function(event) {
            var tecla = (window.event) ? event.keyCode : event.which;

            if (tecla === 13) {

                var Search = $(".Search").val();

                //CHAMA FUN��O AJAX
                $.ajax({
                    type: "POST",
                    url: "php/php.php",
                    data: {
                        Search: Search,
                        acao: "Search"
                    },
                    beforeSend: function() {

                    },
                    complete: function() {

                    },
                    success: function(eSearch) {
                        //RETORNA O TITULO DA PAGINA DE VISUALIZA��O DE PESQUISA, MOSTRADO A INFORMA��O DIGITADA
                        $('#dialog-form-Searh').dialog('option', 'title', 'Localizar por: ' + Search);
                        //RETORNA O QUE FOI ENCONTRADO E MOSTRA NA TABELA
                        $("#dialog-form-Searh table").html(eSearch);
                        //CHAMA O DIALOG JQUERY
                        $("#dialog-form-Searh").dialog("open");
                    }
                });

                return false;
            }


        });
    }
    impedeLetras("#Search");

    //AO CLICAR NO BOT�O PARA SALVAR AS PERMISS�ES PARA O USU�RIO
    $('body').delegate("#SalvarPerfil", "click", function() {

        //PERCORRE TODOS OS CHECKBOX DA <ul>
        $("#VisualizarGrupo").find('.faqGrupoId').each(function() {

            //SE ESTIVER CHECADO
            if (this.checked === true) {

                //ENVIA OS DADOS PARA O PHP PARA INCLUIR PERMISS�O
                $.post("php/php.php", {
                    //INFORMA O ID DO FAQ
                    faqId: $(this).val(),
                    //INFORMA O ID DO USU�RIO
                    idUser: $(this).attr("alt"),
                    acao: "AddPermissao"
                });
                //SE N�O ESTIVER CHECADO
            } else {
                //ENVIA OS DADOS PARA O PHP PARA EXCLUIR PERMISS�O
                $.post("php/php.php", {
                    //INFORMA O ID DO FAQ
                    faqId: $(this).val(),
                    //INFORMA O ID DO USU�RIO
                    idUser: $(this).attr("alt"),
                    acao: "DelPermissao"
                });
            }
        });
        //RETORNA O RESULTADO
        alert('Perf�l Salvo com Sucesso.');

    });

    //AO CLICAR NO BOT�O PARA LOCALIZAR
    $(".SearchBtn").bind("click", function() {
        //RECUPERAR O QUE FOI DIGITADO
        var Search = $(".Search").val();
        //ENVIA PARA O PHP VIA AJAX
        $.ajax({
            type: "POST",
            url: "php/php.php",
            data: {
                Search: Search,
                acao: "Search"
            },
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(eSearch) {
                //SETA O TITULO
                $('#dialog-form-Searh').dialog('option', 'title', 'Localizar por: ' + Search);
                //PREENCHE A TABELA DETRO DO DIALOG
                $("#dialog-form-Searh table").html(eSearch);
                //ABRE O DIALOG JQUERY
                $("#dialog-form-Searh").dialog("open");
            }
        });
    });

    $("#dialog-form-Searh").dialog({
        autoOpen: false,
        height: 500,
        width: 900,
        modal: true,
        buttons: {
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    //AO CLICAR PARA DELETAR A CATEGORIA
    $('body').delegate(".deletarCategoria", "click", function() {

        //CONFIRMA��O PARA DELETAR
        if (confirm("Tem certeza que deseja Deletar esta Categoria?")) {

            //RECEBE O ID DA CATEGORIA QUE EST� NA PRIMEIRA COLUNA
            var CategoriaId = $(this).parent().parent().find('span:first').text();

            //ENVIA OS DADOS PARA O PHP VIA AJAX
            $.post("php/php.php", {
                CategoriaId: CategoriaId,
                acao: "DeletarCategoria"
            }, function(retDeletarCategoria) {
                //NO PHP � VERIFICADO SE A CETEGORIA CONT�M ALGUM FAQ
                if (retDeletarCategoria >= 1) {
                    alert('Categoria N�o Pode Ser Deletada \n Encontrado ' + retDeletarCategoria + ' Faqs para esta categoria.');
                } else {
                    //SE TUDO OK LISTA TODAS AS CATEGORIAS
                    $.post("php/php.php", {
                        acao: 'ListarCategoriasAdmin'
                    }, function(retListarCategoriasAdmin) {
                        if (retListarCategoriasAdmin) {
                            window.setTimeout('location.reload()');
                        }
                    });
                }
            });
        }
    });

    //AO CLICAR PARA EDITA UMA CATEGORIA
    $('body').delegate(".editarCategoria", "click", function() {

        //RECEBE O ID DA CATEGORIA
        var CategoriaId = $(this).parent().parent().find('span:first').text();
        //RECEBE O NOME DA CATEGORIA
        var NomeCategoria = $(this).parent().parent().find('a:first').text();

        //PASSA O ID PARA UM CAMPO OCULTO
        $("#IdCategoria").val(CategoriaId);
        //PASSA O NOME PARA O CAMPO
        $("#EditCategoria").val(NomeCategoria);
        $('#dialog-form-editarCategoria').dialog('option', 'title', 'Editar Categoria');
        $('#dialog-form-editarCategoria').dialog('open');
    });

    $("#dialog-form-editarCategoria").dialog({
        autoOpen: false,
        height: 'auto',
        width: 290,
        modal: true,
        position: ["center", 10],
        buttons: {
            "Salvar": function() {

                //RECEBE O ID DA CATEGORIA
                var CategoriaId = $("#IdCategoria").val();
                //RECEBE O NOME DACATEGORIA
                var NomeCategoria = $("#EditCategoria").val();
                //ENVIA OS DADOS PARA O PH VIA AJAX
                $.post("php/php.php", {
                    CategoriaId: CategoriaId,
                    NomeCategoria: NomeCategoria,
                    acao: 'EditCategoria'
                }, function(e) {
                    //SE TUDO OK LISTA AS CATEGORIAS
                    if (e) {
                        $.post("php/php.php", {
                            acao: 'ListarCategoriasAdmin'
                        }, function(retListarCategoriasAdmin) {
                            if (retListarCategoriasAdmin) {
                                window.setTimeout('location.reload()');
                            }
                        });
                    }
                });
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#addCategoria").click(function() {
        $("#dialog-form-adicionarCategoria").dialog("open");
    });

    $("#dialog-form-adicionarCategoria").dialog({
        autoOpen: false,
        height: 'auto',
        width: 290,
        modal: true,
        position: ["center", "center"],
        buttons: {
            "Salvar": function() {

                var NewCategoria = $("#NewCategoria").val();
                var SelSupCategoria = $('.SelSupCategoria').val();

                if (SelSupCategoria === "") {

                    alert("Informe a Super Categoria");
                } else if (NewCategoria === "") {

                    alert("Informe o Nome da categoria");
                } else {

                    $.post("php/php.php", {
                        NewCategoria: NewCategoria,
                        SelSupCategoria: SelSupCategoria,
                        acao: 'AddCategoria'
                    }, function(e) {
                        if (e) {
                            window.setTimeout('location.reload()');
                        }
                    });
                }
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $("#addSupCategoria").click(function() {
        $("#dialog-form-adicionarSupCategoria").dialog("open");
    });

    $("#dialog-form-adicionarSupCategoria").dialog({
        autoOpen: false,
        height: 'auto',
        width: 300,
        modal: true,
        position: ["center", "center"],
        buttons: {
            "Salvar": function() {

                var NewSupCategoria = $("#NewSupCategoria").val();

                if (NewSupCategoria === "") {

                    alert("Informe o Nome da Super Categoria");
                } else {

                    $.post("php/php.php", {
                        NewSupCategoria: NewSupCategoria,
                        acao: 'AddSupCategoria'
                    }, function(e) {
                        if (e) {
                            window.setTimeout('location.reload()');
                        }
                    });
                }
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $('body').delegate(".editarSupCategoria", "click", function() {

        var editSupCategoria = $("#editSupCategoria").val($(this).parent().parent().find('a:first').text());

        var idEditSupCategoria = $('#idEditSupCategoria').val($(this).parent().parent().find('span:first').text());

        $("#dialog-form-editSupCategoria").dialog('open');


    });

    $("#dialog-form-editSupCategoria").dialog({
        autoOpen: false,
        height: 'auto',
        width: 300,
        modal: true,
        position: ["center", "center"],
        buttons: {
            "Salvar": function() {

                var editSupCategoria = $("#editSupCategoria").val();
                var idEditSupCategoria = $('#idEditSupCategoria').val();

                if (editSupCategoria === "") {

                    alert("Informe o Nome da Super Categoria");
                } else {

                    $.post("php/php.php", {
                        idEditSupCategoria: idEditSupCategoria,
                        editSupCategoria: editSupCategoria,
                        acao: 'editSupCategoria'
                    }, function(eEditSupCategoria) {
                        if (eEditSupCategoria) {
                            window.setTimeout('location.reload()');
                        }
                    });
                }
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    $('body').delegate(".deletarSupCategoria", "click", function() {

        var idSuperCategoria = $(this).parent().parent().find('span:first').text();

        if (confirm("Tem certeza que deseja Deletar esta Super categoria?")) {

            $.post("php/php.php", {
                idSuperCategoria: idSuperCategoria,
                acao: "DelSupCategoria"
            }, function(eDelSupCategoria) {
                if (eDelSupCategoria === '0') {
                    window.setTimeout('location.reload()');
                } else {
                    alert("Super Categoria N�o pode ser Deletada \n Encontrado " + eDelSupCategoria + " Categorias para esta Super Categoria.");
                }

            });
        }


    });

    ///////////////////////////////////////////////////

    $('body').delegate(".deletar", "click", function() {


        if (confirm("Tem certeza que deseja Deletar este Faq?")) {

            var idFaqs = $(this).attr("id");
            var par = $(this).parent().parent();
            var CategoriaId = par.children("td:eq(4)").text();

            $.post("php/php.php", {
                idFaqs: idFaqs,
                acao: "DeletarFaq"
            }, function(retDeletarFaq) {

                if (retDeletarFaq) {
                    window.setTimeout('location.reload()');
                }
            });



        }

    });

    ///////////////////////////////////////////////////

    $('body').delegate(".editar", "click", function() {

        $("#dialog-form-visualizarFaqs").dialog("close");

    });
    //////////////////////////////////////////////////////

    $("body").delegate(".Questao", "click", function() {

        $('.Resposta').slideUp(0.5);
        $('.ExternoLink').slideUp(0.5);
        $('.answer').slideUp(0.5);
        $(this).parent().parent().next('tr').slideToggle();
        $(this).parent().parent().next('tr').next('tr').slideToggle();
        $(this).parent().parent().next('tr').next('tr').next('tr').slideToggle();
        $(this).parent().parent().next('tr').next('tr').next('tr').next('tr').slideToggle();
    });

    $('body').delegate(".visualiz", "click", function() {

        var idCategoria = $(this).attr('alt');
        $.post("php/php.php", {
            idCategoria: idCategoria,
            acao: 'ListarTitulo'
        }, function(tit) {

            $('#dialog-form-visualizarFaqs').dialog('option', 'title', tit);
        });

        $.post("php/php.php", {
            idCategoria: idCategoria,
            acao: 'ListarQuestoes'
        }, function(e) {

            $("#dialog-form-visualizarFaqs table").html(e);
        });

        $("#dialog-form-visualizarFaqs").dialog("open");
    });

    $("#dialog-form-visualizarFaqs").dialog({
        autoOpen: false,
        height: 'auto',
        width: 900,
        modal: true,
        position: ["center", 10],
        buttons: {
            "Fechar": function() {
                $("#dialog-form-visualizarFaqs table").html("");
                $(this).dialog("close");
            }
        }

    });
});