<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['MM_UserId'])):
    header("Location: login/index.php");
endif;
if (!isset($_GET['link'])) {

} else {
    ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <?php date_default_timezone_set("Brazil/East"); ?>
            <meta http-equiv="Content-Type" content="text/html; charset=iso 8859-9" />
            <title>Siga TI - Manual Normativo</title>
            <link href="css/style.css" type="text/css" rel="stylesheet" />
            <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
            <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>
            <script type="text/javascript" src="js/js.js"></script>
            <script src="jquery.form.js" type="text/javascript"></script>
            <script src="upload.js" type="text/javascript"></script>
            <link type="text/css" href="css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
        </head>

        <body>
            <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="55" class="colorbar">
                        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" width="50%"><a href="index.php"><img src="http://www.secureoffice.com.br/eSIGA/logos/vertical-tradicional.bmp" width="100" height="105"></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="5" class="colorbar2"></td>
                </tr>
                <tr>
                    <td height="480px" class="maincontent" valign="top">
                        <?php
                        include 'Connections/DB.class.php';
                        include 'classes/getSet.class.php';
                        include 'classes/functions.class.php';
                        $Functions = new functions();
                        ?>
                        <div id="box">
                            <table width="100%" border="0">
                                <?php
                                $Functions->setLinkExterno($_GET['link']);
                                $Functions->setUser_id($_SESSION['MM_UserId']);
                                $Functions->LinkInterno();
                                foreach ($Functions->LinkInterno() as $dados) {

                                    //VERIFICA O TIPO DE RESPOSTA
                                    if ($dados['answer'] == 'Clique aqui para ver a Resposta!'):
                                        //SE TIVER A RESPOSTA 'Clique aqui para ver a Resposta!' VAI APARECER O LINK PARA O PDF
                                        $Link = '<a href="admin/uploads/uploads/' . $dados['category_id'] . '/' . $dados['link'] . '" target="_blank" class=".pdf">Clique aqui para ver a Resposta!</a>';
                                    else:
                                        //SE FOR OUTRO TIPO DE RESPOSTA, MOSTRA A RESPOSTA NORMAL
                                        $Link = $dados['answer'];
                                    endif;

                                    echo '<tr class="question">
                                            <td width="5%"><h1>Q</h1></td>
                                            <td width="95%"><a href="javascript:void(0);" class="question Questao">' . htmlentities($dados['question'], HTML_ENTITIES) . '</a></td>
                                    </tr>
                                    <tr class="answer Resposta">
                                            <td><h1>R</h1></td>
                                            <td style="font-size:13px;">' . $Link . '</td>
                                    </tr>';
                                }
                                ?></table>
                        </div>

                        <!-- End Main Mody -->
                    </td>
                </tr>
                <tr>
                    <td height="1" class="hrz_line"></td>
                </tr>
                <tr>
                    <td height="50" align="right" class="foot">Powered by <a href="http://www.netbizcity.com" target="_blank">NetBizCity</a>. Copyright &copy; 2007.</td>
                </tr>
            </table>
        </body>
    </html>
    <?php
}
?>
