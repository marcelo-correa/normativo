<?php

class functions extends getSet
{

    public function ListarFaqsBySupCategoria() {
        $sql = "SELECT * FROM faqs WHERE supCategoria_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCategoria_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarFaqsByCategoria() {
        $sql = "SELECT * FROM faqs WHERE category_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function AtualizarEmailEnviadoPermissao() {
        $sql = "UPDATE permission SET email_enviado = '1' WHERE faqs_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id() );
        return $query->execute( $data );
    }

    public function ListarPermissionFaqsUserByFaqId() {
        $sql = "SELECT * FROM permission LEFT JOIN faqs ON faqs.id = permission.faqs_id LEFT JOIN users ON users.idUser = permission.user_id ";
        $sql .= "WHERE faqs_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarPermissaoSemEmail() {
        $sql = "SELECT * FROM permission LEFT JOIN faqs ON permission.faqs_id = faqs.id WHERE email_enviado = '0' AND user_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function UpdateEmailEnviado() {
        $sql = "UPDATE permission SET email_enviado = 1 WHERE user_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        return $query->execute( $data );
    }

    public function VerificarSenhaAtual() {
        $sql = "SELECT * FROM users WHERE passwrd = ? AND idUser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getPasswrd(), $this->getIdUser() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function AlterarSenha() {
        $sql = "UPDATE users SET passwrd = ? WHERE iduser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getPasswrd(), $this->getIdUser() );
        return $query->execute( $data );
    }

    public function addUsuario() {
        $sql = "INSERT INTO users SET userid = ?, passwrd = ?, nomeUser = ?, nivel = ?, email = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUserid(), $this->getPasswrd(), $this->getNomeUser(), $this->getNivel(), $this->getEmail() );
        return $query->execute( $data );
    }

    public function duplicidadeUsuario() {
        $sql = "SELECT * FROM users WHERE userid = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUserid() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function EditUsuario() {
        $sql = "UPDATE users SET userid = ?, nomeUser = ?, nivel = ?, grupoUser = ?, statusUser = ?, email = ? WHERE iduser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUserid(), $this->getNomeUser(), $this->getNivel(), $this->getGrupoUser(), $this->getStatusUser(), $this->getEmail(), $this->getIdUser() );
        return $query->execute( $data );
    }

    public function DelUsuario() {
        $sql = "DELETE FROM users WHERE iduser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getIdUser() );
        return $query->execute( $data );
    }

    public function ListarUltUsuario() {
        $sql = "SELECT MAX(idUser) AS UltUser FROM users";
        return $query = DB::conectar()->query( $sql );
    }

    public function ListarUsuarios() {
        $sql = "SELECT * FROM users";
        return $query = DB::conectar()->query( $sql );
    }

    public function SelecionarUsuario() {
        $sql = "SELECT * FROM users WHERE idUser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getIdUser() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarPermission() {
        $sql = "SELECT * FROM permission WHERE faqs_id = ? AND user_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id(), $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function AddPermission() {
        $sql = "INSERT INTO permission SET faqs_id = ?, user_id = ?, categoria_id = ?, SupCat_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id(), $this->getUser_id(), $this->getCategoria_id(), $this->getSupCat_id() );
        return $query->execute( $data );
    }

    public function EditPermission() {
        $sql = "UPDATE permission SET faqs_id = ?, user_id = ?, status = ?, grupoPermissionUser = ? WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id(), $this->getUser_id(), $this->getStatus(), $this->getGrupoPermissionUser(), $this->getPermiss_id() );
        return $query->execute( $data );
    }

    public function EditPermissionByFaq() {
        $sql = "UPDATE permission SET categoria_id = ?, SupCat_id = ? WHERE faqs_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategoria_id(), $this->getSupCat_id(), $this->getFaqs_id() );
        return $query->execute( $data );
    }

    public function ListarPermissionAll() {
        $sql = "SELECT * FROM permission WHERE faqs_id = ? AND grupoPermissionUser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id(), $this->getGrupoPermissionUser() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function DelPermission() {
        $sql = "DELETE FROM permission WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getPermiss_id() );
        return $query->execute( $data );
    }

    public function DelPermissionByFaq() {
        $sql = "DELETE FROM permission WHERE faqs_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqs_id() );
        return $query->execute( $data );
    }

    public function DelPermissionByUsuario() {
        $sql = "DELETE FROM permission WHERE user_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        return $query->execute( $data );
    }

    public function DelPermissionByUsuarioAndCategory() {
        $sql = "DELETE FROM permission WHERE user_id = ? AND categoria_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id(), $this->getCategoria_id() );
        return $query->execute( $data );
    }

    public function DelPermissionByUsuarioAndSupCategory() {
        $sql = "DELETE FROM permission WHERE user_id = ? AND supCat_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id(), $this->getSupCat_id() );
        return $query->execute( $data );
    }

    public function AddFaq() {
        $sql = "INSERT INTO faqs SET category_id = ?, supCategoria_id = ?, question = ?, answer = ?, link = ?, linkExterno = ?, dataCadastro = NOW(), userInclusao = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id(), $this->getSupCategoria_id(), $this->getQuestion(), $this->getAnswer(), $this->getLink(), $this->getLinkExterno(), $this->getUserInclusao() );
        return $query->execute( $data );
    }

    public function DeletarFaq() {
        $sql = "DELETE FROM faqs WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaq_id() );
        return $query->execute( $data );
    }

    public function SelectUltFaq() {
        $sql = "SELECT max(id) AS UltFaq FROM faqs";
        return $query = DB::conectar()->query( $sql );
    }

    public function AddCategoria() {
        $sql = "INSERT INTO faqcategories SET category = ?, SCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory(), $this->getSCatId() );
        return $query->execute( $data );
    }

    public function ListarUltCategoria() {
        $sql = "SELECT MAX(id) AS UltCategoria FROM faqcategories";
        return DB::conectar()->query( $sql );
    }

    public function EditCategoria() {
        $sql = "UPDATE faqcategories SET category = ? WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory(), $this->getFaqsCat_id() );
        return $query->execute( $data );
    }

    public function VerificarCategoriaEmUso() {
        $sql = "SELECT * FROM faqs WHERE category_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function DeletarCategoria() {
        $sql = "DELETE FROM faqcategories WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqsCat_id() );
        return $query->execute( $data );
    }

    public function ListarCategorias() {
        $sql = "SELECT F.id AS Id_faqs, category_id, question, answer, FC.id AS Id_category, category, P.id AS Id_permission, faqs_id, `status`, user_id FROM faqs as F LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN SupCategoria ON supCatId = supCategoria_id LEFT JOIN permission as P ON P.faqs_id = F.id ";
        $sql .= "WHERE P.`status` = 1 AND P.user_id = ? GROUP BY category";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriasSelect() {
        $sql = "SELECT * FROM faqs as F LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN permission as P ON P.faqs_id = F.id ";
        $sql .= "WHERE P.`status` = 1 AND P.user_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriasAdmin() {
        $sql = "SELECT * FROM faqcategories WHERE SCatId = ? GROUP BY category ORDER BY id";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSCatId() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriasById() {
        $sql = "SELECT * FROM faqcategories WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaqsCat_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriasAdmin2() {
        $sql = "SELECT * FROM faqcategories GROUP BY category ORDER BY id";
        return DB::conectar()->query( $sql );
    }

    public function ListarCategoriasSelectDiferente() {
        $sql = "SELECT * FROM faqcategories where id != ? AND SCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id(), $this->getSCatId() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarTituloId() {
        $sql = "SELECT * FROM faqcategories WHERE id = ? GROUP BY category";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarFaqs() {
        $sql = "SELECT * FROM faqs";
        return DB::conectar()->query( $sql );
    }

    public function ListarIdFaq() {
        $sql = "SELECT F.id AS FaqId, F.category_id AS category_id, F.supCategoria_id AS supCategoria_id, F.question AS question, F.answer AS answer, ";
        $sql .= "F.link AS link, F.linkExterno AS linkExterno, F.dataCadastro AS dataCadastro, F.dataModificado AS dataModificado, F.userInclusao AS ";
        $sql .= "userInclusao, F.userModificado AS userModificado, FC.id AS CategoryId, FC.category AS category, FC.SCatId AS SCatId, SC.supCatId AS ";
        $sql .= "supCatId, SC.supCatNome AS supCatNome, P.id AS PermissId, P.faqs_id, P.`status`, P.user_id, P.grupoPermissionUser FROM faqs as F ";
        $sql .= "LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN SupCategoria AS SC ON supCatId = supCategoria_id LEFT JOIN permission AS ";
        $sql .= "P ON P.faqs_id = F.id WHERE F.id = ? GROUP BY F.id";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getFaq_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarQuetoesIdCategoria() {
        $sql = "SELECT F.id, category_id, supCategoria_id, question, answer, link, linkExterno, dataCadastro, dataModificado, userInclusao, ";
        $sql .= "UInclusao.nomeUser AS UserInclusao, userModificado, UModificado.nomeUser AS UserModificado, category, faqs_id, `status`, user_id FROM ";
        $sql .= "faqs AS F LEFT JOIN faqcategories AS FC ON FC.id = category_id LEFT JOIN permission AS P ON P.faqs_id = F.id LEFT JOIN users AS U ON ";
        $sql .= "U.idUser = P.user_id LEFT JOIN users AS UInclusao ON UInclusao.idUser = userInclusao LEFT JOIN users AS UModificado ON ";
        $sql .= "UModificado.idUser = userModificado WHERE P.`status` = 1 AND F.category_id = ? AND U.idUser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id(), $this->getIdUser() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarQuetoesBySearch() {
        $sql = "SELECT F.id, category_id, supCategoria_id, question, answer, link, linkExterno, dataCadastro, dataModificado, userInclusao, ";
        $sql .= "UInclusao.nomeUser AS UserInclusao, userModificado, UModificado.nomeUser AS UserModificado, category, faqs_id, `status`, user_id FROM ";
        $sql .= "faqs AS F LEFT JOIN faqcategories AS FC ON FC.id = category_id LEFT JOIN permission AS P ON P.faqs_id = F.id LEFT JOIN users AS U ON ";
        $sql .= "U.idUser = P.user_id LEFT JOIN users AS UInclusao ON UInclusao.idUser = userInclusao LEFT JOIN users AS UModificado ON ";
        $sql .= "UModificado.idUser = userModificado ";
        $sql .= "WHERE P.`status` = 1 AND MATCH(F.question, F.answer) AGAINST( ? ) AND U.idUser = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getQuestion(), $this->getIdUser() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function AlterarFaq() {
        $sql = "UPDATE faqs SET category_id = ?, supCategoria_id = ?, question = ?, answer = ?, link = ?, linkExterno = ?, userModificado = ?, dataModificado = NOW() WHERE id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getCategory_id(), $this->getSupCategoria_id(), $this->getQuestion(), $this->getAnswer(), $this->getLink(), $this->getLinkExterno(), $this->getUserModificado(), $this->getFaq_id() );
        return $query->execute( $data );
    }

    public function ListarCategoriasFaqsNaoPermitidos() {
        $sql = "SELECT faqs.id AS id_faqs, category_id, category, question, answer FROM faqs LEFT JOIN faqcategories ON faqcategories.id = category_id ";
        $sql .= "WHERE faqs.id NOT IN(SELECT permission.faqs_id FROM permission WHERE permission.user_id = ?) GROUP BY category";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarFaqsNaoPermitidos() {
        $sql = "SELECT faqs.id AS id_faqs, category_id, category, question, answer FROM faqs LEFT JOIN faqcategories ON faqcategories.id = category_id ";
        $sql .= "WHERE faqs.id NOT IN(SELECT permission.faqs_id FROM permission WHERE permission.user_id = ?) AND category_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id(), $this->getCategory_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarFaqsPermitidos() {
        $sql = "SELECT faqs.id AS id_faqs, category_id, category, question, answer FROM faqs LEFT JOIN faqcategories ON faqcategories.id = category_id ";
        $sql .= "LEFT JOIN permission ON permission.faqs_id = faqs.id WHERE permission.user_id = ? AND category_id = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id(), $this->getCategory_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function LinkExterno() {
        $sql = "SELECT F.id, category_id, supCategoria_id, question, answer, link, linkExterno, category FROM faqs as F LEFT JOIN faqcategories as ";
        $sql .= "FC ON FC.id = category_id WHERE linkExterno = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getLinkExterno() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function LinkInterno() {
        $sql = "SELECT * FROM permission LEFT JOIN faqs ON faqs.id = permission.faqs_id WHERE linkExterno = ? AND ";
        $sql .= "user_id = ? GROUP BY faqs.id";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getLinkExterno(), $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function AddSupCategoria() {
        $sql = "INSERT INTO SupCategoria SET supCatNome = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCatNome() );
        return $query->execute( $data );
    }

    public function EditSupCategoria() {
        $sql = "UPDATE SupCategoria SET supCatNome = ? WHERE supCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCatNome(), $this->getSupCatId() );
        return $query->execute( $data );
    }

    public function DelSupCategoria() {
        $sql = "DELETE FROM SupCategoria WHERE supCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCatId() );
        return $query->execute( $data );
    }

    public function verificarSupCategoriaEmUso() {
        $sql = "SELECT * FROM faqcategories WHERE SCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCategoria_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarSuperCategoria() {
        $sql = "SELECT * FROM SupCategoria";
        return DB::conectar()->query( $sql );
    }

    public function ListarSuperCategoriaSelecDiferente() {
        $sql = "SELECT * FROM SupCategoria WHERE supCatId != ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCatId() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriaBySupCategoria() {
        $sql = "SELECT * FROM faqcategories WHERE SCatId = ?";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSCatId() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarFaqBySupCategoria() {
        $sql = "SELECT * FROM faqs as F LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN SupCategoria ON supCatId = supCategoria_id ";
        $sql .= "LEFT JOIN permission as P ON P.faqs_id = F.id WHERE P.`status` = 1 AND supCategoria_id = ? AND user_id = ? GROUP BY F.id";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getSupCategoria_id(), $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarSuperCategoriasPermitidasGroupBySupCatId() {
        $sql = "SELECT * FROM faqs as F LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN SupCategoria ON supCatId = supCategoria_id ";
        $sql .= "LEFT JOIN permission as P ON P.faqs_id = F.id WHERE P.`status` = 1 AND user_id = ? GROUP BY supCatId";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id() );
        $query->execute( $data );
        return $query->fetchAll();
    }

    public function ListarCategoriasPermitidasGroupByCategory() {
        $sql = "SELECT * FROM faqs as F LEFT JOIN faqcategories as FC ON FC.id = category_id LEFT JOIN SupCategoria ON supCatId = supCategoria_id ";
        $sql .= "LEFT JOIN permission as P ON P.faqs_id = F.id WHERE P.`status` = 1 AND user_id = ? AND SCatId = ? GROUP BY category ORDER BY category_id";
        $query = DB::conectar()->prepare( $sql );
        $data = array( $this->getUser_id(), $this->getSCatId() );
        $query->execute( $data );
        return $query->fetchAll();
    }

}

?>
