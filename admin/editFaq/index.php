<?php
if (!isset($_SESSION)) {
    session_start();
}
include './restricaoAdmin.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php date_default_timezone_set("Brazil/East"); ?>
        <meta http-equiv="Content-Type" content="text/html; charset=iso 8859-9" />
        <title>Siga TI - BANCO DE CONHECIMENTO</title>
        <link href="../../css/style.css" type="text/css" rel="stylesheet" />
        <link type="text/css" href="../../css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
            <script src="jquery.js" type="text/javascript"></script>
            <script src="jquery.form.js" type="text/javascript"></script>
            <script src="upload.js" type="text/javascript"></script>
            <style type="text/css">
                body{
                    font-size: 16px;
                }
            </style>
    </head>
    <body>
        <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td height="55" class="colorbar">
                    <table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" width="10%"><img src="http://www.secureoffice.com.br/eSIGA/logos/vertical-tradicional.bmp" width="100" height="105"></td>
                            <td><h1>BANCO DE CONHECIMENTO</h1></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="5" class="colorbar2"></td>
            </tr>
            <tr>
                <td height="350px" class="maincontent" valign="top">
                    <div id="dialog-form-adicionarFaq" class="AddAlt" style="width: 900px;">
                        <fieldset>
                            <?php
                            include '../../Connections/DB.class.php';
                            require '../../classes/carregaClassAdmin2.php';
                            $Functions = new functions();
                            $Functions->setFaq_id($_GET['faq']);
                            //CHAMA A FUN��O
                            $Functions->ListarIdFaq();
                            //RETORNA O ARRAY
                            foreach ($Functions->ListarIdFaq() as $Faq) {

                                echo '<legend>' . $Faq['category'] . '</legend>
                                <form name="adicionarFaq" action="" method="post">
                                <input type="hidden" id="CategoriaId" class="CategoriaId" value="' . $Faq['category_id'] . '"/>
                                <input type="hidden"id="SupCategoriaId" class="SupCategoriaId" value="' . $Faq['supCategoria_id'] . '"/>';

                                echo '<label style="width: 210px;">
                                <span>Super Categoria</span>
                                <select id="SupCatFaq" class="SupCatFaq">
                                <option value="' . $Faq['supCatId'] . '">' . $Faq['supCatNome'] . '</option>';
                                $Functions->setSupCatId($Faq['supCatId']);
                                $Functions->ListarSuperCategoriaSelecDiferente();
                                foreach ($Functions->ListarSuperCategoriaSelecDiferente() as $dados) {
                                    echo '<option value="' . $dados['supCatId'] . '">' . $dados['supCatNome'] . '</option>';
                                }
                                echo'</select>
                                </label>';

                                echo '<label style="width: 50%;">
                                <span>Categoria:</span>
                                <select id="CatFaq" class="CatFaq">
                                <!--MOSTRA A CATEGORIA ATUAL-->
                                <option value="' . $Faq['category_id'] . '">' . $Faq['category'] . '</option>';
                                //RECEBE O ID DA CATEGORIA ATUAL
                                $Functions->setCategory_id($Faq['category_id']);

                                $Functions->setSCatId($Faq['supCatId']);
                                //CHAMA A FUN��O PARA MOSTRAR AS CATEGORIAS RESTANTES MENOS A CATEGORIA ATUAL
                                $Functions->ListarCategoriasSelectDiferente();
                                //RETORNA O ARRAY
                                foreach ($Functions->ListarCategoriasSelectDiferente() as $Cat) {
                                    echo '<option value="' . $Cat['id'] . '">' . $Cat['category'] . '</option>';
                                }
                                echo '</select>
                                </label>';
                                //RETORNA O ID DO FAQ
                                echo '<input type="hidden" id="F_Id" class="F_Id" value="' . $Faq['FaqId'] . '">';
                                //ARMAZENA O ID DA CATEGORIA ATUAL PARA QUE SE O USUARIO TROCAR O FAQ DE CATEGORIA
                                //NO MOMENTO EM QUE SALVAR PODER CHAMAR A LISTAGEM PELA CATEGORIA ANTERIOR
                                echo '<input type="hidden" id="Fixo" class="Fixo" value="' . $Faq['category_id'] . '">
                                <input type="hidden" id="arquivo_antigo" class="arquivo_antigo" value="' . $Faq['link'] . '">
                                <label style="width: 100%; float: left;">
                                <span style="width: 100%; font-size: 16px;">Pergunta:</span>
                                <textarea id="Pergunta" class="Pergunta" style="width: 100%; float: left">' . $Faq['question'] . '</textarea>
                                </label>
                                <label style="width: 100%; float: left;">
                                <span style="width: 100%; font-size: 16px;">Resposta:</span>
                                <textarea id="RespostaFaq" class="Resposta" style="width: 100%; float: left">' . $Faq['answer'] . '</textarea>
                                </label>
                                <label>
                                <span>Nome do Arquivo:</span>
                                <input type="text" id="Arquivo" class="NomeArquivo" readonly name="NomeArquivo" value="' . $Faq['link'] . '">
                                </label>';
                            }
                            ?>


                            </form>
                            <form name="formUpload" id="formUpload" class="formUpload" method="post">
                                <label>Selecione o arquivo: <br> <input type="file" name="arquivo" class="arquivo" id="arquivo" size="45" /></label>
                                <br />
                                <progress value="0" max="100"></progress><span id="porcentagem">0%</span>
                                <br />
                                <input type="hidden" class="CatUpload" name="CatUpload" value="<?php echo $Faq['category_id'] ?>">
                                <!--<input type="button" id="btnEnviar" class="btnEnviar" value="Enviar Arquivo" />-->
                            </form>
                            <div id="resposta" class="resposta">

                            </div>
                            <div class="btn" style="float: left; width: 850px; margin-top: 10px;">
                                <input type="checkbox" name="enviar_email" checked class="enviar_email" style="float: left; width: 15px; margin-right: 5px; padding: 0;"></input>Enviar Email aos Usu�rios com Permiss�o
                                <input type="button" value="Fechar" name="fechar" class="fechar" id="fechar" style="float: right; width: 100px;">
                                    <input type="button" value="Salvar" name="EditFaq" class="EditarFaqedit" id="EditarFaqedit" style="float: right; width: 100px;">

                                        </div>

                                        </fieldset>

                                        </div>



                                        <!-- End Main Mody -->
                                        </td>
                                        </tr>
                                        <tr>
                                            <td height="1" class="hrz_line"></td>
                                        </tr>
                                        <tr>
                                            <td height="50" align="right" class="foot">Desenvolvido por <a href="http://www.sigati.com.br" target="_blank">SIGA TI Tecnologia da Informa&ccedil;&atilde;o</a>. Copyright &copy; 2013 - <?php echo date('Y') ?>.</td>
                                        </tr>
                                        </table>
                                        </body>
                                        </html>