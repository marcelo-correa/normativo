<?php

$Arquivo = $_POST['arquivo'];

$extensoesPermitidas = array("pdf", "PDF");

//  Quebra pelo ponto
$partes = explode(".", $Arquivo);

// Pega somente a ultima parte
$extensao = array_pop($partes);

// se a extensao nao estiver na lista de permitidas, termina o script
if (in_array($extensao, $extensoesPermitidas) == false):
    echo $extensao;
else:
    echo 1;
endif;
?>
