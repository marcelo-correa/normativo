<?php

//INICIAR SESS�O
if (!isset($_SESSION)) {
    session_start();
}
//SE HORA FOR ANTES DO MEIO DIA
if (date('H') < 12):
    echo 'Bom dia <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
//SE HORA FOR ANTES DAS 19 HORAS
elseif (date('H') < 19):
    echo 'Boa tarde <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
//SE HORA FOR ANTES DAS 00:00
else:
    echo 'Boa Noite <strong style="font-weight:bold">' . $_SESSION['MM_UserNome'] . '</strong><br>';
endif;
?>