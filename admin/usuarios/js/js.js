$(document).ready(function() {

    $("#MarcarEmailEnviado").bind("click", function() {

        $.ajax({
            type: "POST",
            url: "php/php.php",
            async: false,
            data: {
                getIdUser: $('.getIdUser').val(),
                acao: "MarcarComoEnviado"
            }
        });
    });

    $("#VisualizarGrupo").delegate(".CheckSupCategoria", "click", function() {

        var SuperCategoriaId = $(this).val();
        var UsuarioId = $(this).attr("alt");

        if (this.checked === true) {

            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    SuperCategoriaId: SuperCategoriaId,
                    UsuarioId: UsuarioId,
                    acao: "AddPermissaoBySupCategoria"
                }
            });

        } else {

            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    SuperCategoriaId: SuperCategoriaId,
                    UsuarioId: UsuarioId,
                    acao: "DelPermissaoBySupCategoria"
                }
            });
        }
    });

    $("#VisualizarGrupo").delegate(".CheckCategoria", "click", function() {

        var SuperCategoriaId = $(this).parent().parent().parent().find('span').text();



        if (this.checked === true) {

            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    SuperCategoriaId: SuperCategoriaId,
                    CategoriaId: $(this).val(),
                    idUser: $(this).attr("alt"),
                    acao: "AddPermissaoByCategoria"
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    SuperCategoriaId: SuperCategoriaId,
                    CategoriaId: $(this).val(),
                    idUser: $(this).attr("alt"),
                    acao: "DelPermissaoByCategoria"
                }
            });
        }
    });

    $("#VisualizarGrupo").delegate(".faqGrupoId", "click", function() {

        if (this.checked === true) {

            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    faqId: $(this).val(),
                    idUser: $(this).attr("alt"),
                    acao: "AddPermissao"
                }
            });
        } else {
            $.ajax({
                type: "POST",
                url: "php/php.php",
                async: false,
                data: {
                    faqId: $(this).val(),
                    idUser: $(this).attr("alt"),
                    acao: "DelPermissao"
                }
            });
        }
    });


    function EnviarEmail() {

        $.ajax({
            type: "POST",
            url: "php/php.php",
            async: false,
            data: {
                getIdUser: $('.getIdUser').val(),
                acao: "BuscarDadosEmail"
            },
            success: function(eBuscarDadosEmail) {

                if (eBuscarDadosEmail !== '0') {

                    $.ajax({
                        type: "POST",
                        url: "php/php.php",
                        async: false,
                        data: {
                            getIdUser: $('.getIdUser').val(),
                            msg: eBuscarDadosEmail,
                            acao: "EnviarEmail"
                        },
                        success: function(eEnviarEmail) {
                            if (eEnviarEmail === '1') {

                                alert('Email enviado com Sucesso!');
                            }
                        }
                    });
                }
            }
        });

    }

    //AO CLICAR NO BOT�O PARA SALVAR O PERF�L
    $('body').delegate("#SalvarPerfil", "click", function() {


        EnviarEmail();

//        window.setTimeout(1000);
//
//        alert('Perf�l Salvo com Sucesso.');
//
        window.setTimeout('location.reload()', 500);

    });

    //ATIVA O TREVIEW, SANFONA COM CHECKBOX
    $('#VisualizarGrupo').checkboxTree({
        initializeChecked: 'collapsed',
        initializeUnchecked: 'collapsed'
    });

    //AO DELETAR USUARIO
    $('body').delegate(".deletarUsuario", "click", function() {

        //CONFIRMACAO
        if (confirm("Tem certeza que deseja Deletar este Usu�rio?")) {
            var par = $(this).parent().parent();
            //RECUPERA O ID DO USUARIO
            var UsuarioId = par.children("td:eq(0)").text();
            //ENVIA OS DADOS DO USUARIO
            //PARA OPH VIA AJAX
            $.ajax({
                url: "php/php.php",
                type: "POST",
                data: {
                    UsuarioId: UsuarioId,
                    acao: "DelUsuario"
                },
                beforeSend: function() {

                },
                complete: function() {

                },
                success: function(eDelUsuario) {
                    if (eDelUsuario) {
                        window.setTimeout('location.reload()');
                    }
                }
            });
        }
    });

    //VISUALIZAR CADASTRO DO USUARIO
    $('body').delegate(".visualiz", "click", function() {

        var par = $(this).parent().parent();
        //RECUPERA O ID DO USUARIO
        var UsuarioId = par.children("td:eq(0)").text();
        //RECEBE O NOME DO USUARIO
        var UsuarioNome = par.children("td:eq(1)").text();

        //ENVIA OS DADOS PARA O PHP VIA AJAX
        $.ajax({
            url: "php/php.php",
            type: "POST",
            data: {
                UsuarioId: UsuarioId,
                acao: "SelecionarUsuario"
            },
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function(eSelecionarUsuario) {
                //PREENCHE O DIALOG JQUERY
                $('#dialog-form-editarUsuario fieldset').html(eSelecionarUsuario);
                //PREENCHE O TITULO DO DIALOG JQUERY
                $('#dialog-form-editarUsuario').dialog('option', 'title', 'Editar Usu�rio ' + UsuarioNome);
                //ABRE O DIALOG JQUERY
                $('#dialog-form-editarUsuario').dialog('open');
            }
        });

    });

    $("#dialog-form-editarUsuario").dialog({
        autoOpen: false,
        height: 'auto',
        width: 500,
        modal: true,
        buttons: {
            "Salvar": function() {

                var IdUser = $(".IdUser").val();
                var LoginUser = $(".LoginUser").val();
                var NomeUser = $(".NomeUser").val();
                var NivelUser = $(".NivelUser").val();
                var StatusUser = $(".StatusUser").val();
                var EmailUser = $(".EmailUser").val();

                if (LoginUser === "" && IdUser === "" && StatusUser === "" && NomeUser === "" && NivelUser === "") {
                    $("#dialog-form-editarUsuario").dialog("close");
                } else if (LoginUser === "") {
                    alert("Informe o Login");
                } else if (IdUser === "") {
                    alert("Usu�rio n�o Identificado");
                } else if (NomeUser === "") {
                    alert("Informe o Nome");
                } else if (EmailUser === "") {
                    alert("Informe o Email");
                } else {

                    $.post("php/php.php", {
                        IdUser: IdUser,
                        LoginUser: LoginUser,
                        NomeUser: NomeUser,
                        NivelUser: NivelUser,
                        StatusUser: StatusUser,
                        EmailUser: EmailUser,
                        acao: "EditUsuario"
                    }, function(retEditUsuario) {
                        if (retEditUsuario) {

                            window.setTimeout('location.reload()');
                        }
                    });
                }
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    //AO ADICIONAR UM USUARIO
    $('body').delegate("#addUsuario", "click", function() {
        //ABRE O DIALOG JQUERY COM O FORMULARIO
        $('#dialog-form-adicionarUsuario').dialog('open');
    });

    $("#dialog-form-adicionarUsuario").dialog({
        autoOpen: false,
        height: 'auto',
        width: 500,
        modal: true,
        buttons: {
            "Salvar": function() {

                var LoginUser = $(".LoginUser").val();
                var SenhaUser = $(".SenhaUser").val();
                var ConfSenhaUser = $(".ConfSenhaUser").val();
                var NomeUser = $(".NomeUser").val();
                var NivelUser = $(".NivelUser").val();
                var EmailUser = $(".EmailUser").val();

                if (LoginUser === "" && SenhaUser === "" && ConfSenhaUser === "" && NomeUser === "" && NivelUser === "") {
                    $("#dialog-form-adicionarUsuario").dialog("close");
                } else if (LoginUser === "") {
                    alert("Informe o Login");
                } else if (SenhaUser === "") {
                    alert("Informe a Senha");
                } else if (ConfSenhaUser === "") {
                    alert("Confirme a Senha");
                } else if (SenhaUser !== ConfSenhaUser) {
                    alert("As Senhas n�o Conferem");
                } else if (NomeUser === "") {
                    alert("Informe o Nome");
                } else if (EmailUser === "") {
                    alert("Informe o Email");
                } else {

                    $.post("php/php.php", {
                        LoginUser: LoginUser,
                        SenhaUser: SenhaUser,
                        NomeUser: NomeUser,
                        NivelUser: NivelUser,
                        EmailUser: EmailUser,
                        acao: "AddUsuario"
                    }, function(retAddUsuario) {

                        if (retAddUsuario === '1') {
                            window.setTimeout('location.reload()');
                        } else {
                            alert('Usu�rio j� Cadastrado!');
                        }
                    });
                }
            },
            "Fechar": function() {
                $(this).dialog("close");
            }
        }
    });

    function LimparCampos(obj) {

        $(obj).find("input").each(function() {
            $(this).val("");
        });
    }
});