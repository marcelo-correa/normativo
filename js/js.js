$( document ).ready( function() {

    $( ".liSuper" ).click( function() {

        $( ".liSuper" ).parent().children().find( 'li' ).slideUp();

        $( this ).parent().children().find( 'li' ).slideToggle();

    } );

    function impedeLetras( obj ) {
        $( obj ).keypress( function( event ) {
            var tecla = ( window.event ) ? event.keyCode : event.which;

            if ( tecla === 13 ) {

                var Search = $( ".Search" ).val();

                //CHAMA FUN��O AJAX
                $.ajax( {
                    type: "POST",
                    url: "php/php.php",
                    data: {
                        Search: Search,
                        acao: "Search"
                    },
                    beforeSend: function() {

                    },
                    complete: function() {

                    },
                    success: function( eSearch ) {
                        //RETORNA O TITULO DA PAGINA DE VISUALIZA��O DE PESQUISA, MOSTRADO A INFORMA��O DIGITADA
                        $( '#dialog-form-Searh' ).dialog( 'option', 'title', 'Localizar por: ' + Search );
                        //RETORNA O QUE FOI ENCONTRADO E MOSTRA NA TABELA
                        $( "#dialog-form-Searh table" ).html( eSearch );
                        //CHAMA O DIALOG JQUERY
                        $( "#dialog-form-Searh" ).dialog( "open" );
                    }
                } );

                return false;
            }


        } );
    }
    impedeLetras( "#Search" );
    //AO CLICAR PARA ADICIONAR UM FAQ
    $( 'body' ).delegate( ".AdicionarFaq", "click", function() {

        //RECEBE O ID DA CATEGORIA
        var CategoriaId = $( ".CategoriaId" ).val();
        //RECEBE O NOME DA CATEGORIA
        var SupCategoriaId = $( ".SupCategoriaId" ).val();

        //INFORMA AO CAMPO OCULTO O ID DA CATEGORIA
        //CAMPO OCULTO QUE INDICA PARA QUAL PASTA DEVER� SER FEITO O UPLOAD
        //SE FOR FEITO
        $( ".CatUpload" ).val( CategoriaId );
        //RECEBE O ID DA CATEGORIA
        $( ".CategoriaId" ).val( CategoriaId );
        $( '#dialog-form-adicionarFaq' ).dialog( 'open' );
    } );

    function Upload() {

        $( '.formUpload' ).ajaxForm( {
            uploadProgress: function( event, position, total, percentComplete ) {
                $( 'progress' ).attr( 'value', percentComplete );
                $( '.porcentagem' ).html( percentComplete + '%' );
            },
            success: function( data ) {
                $( 'progress' ).attr( 'value', '100' );
                $( '.porcentagem' ).html( '100%' );
                if ( data.sucesso === true ) {
                    $( ".NomeArquivo" ).val( data.NomeArquivo );
                    $( ".NovaResposta" ).val( 'Clique aqui para ver a Resposta!' );
                    $( '.resposta' ).html( 'Arquivo enviado com Sucesso!' );

                    SalvarFaq();
                }
                else {
                    $( '.resposta' ).html( data.msg );
                }
            },
            error: function() {
                $( '.resposta' ).html( 'Erro ao enviar requisi��o!!!' );
            },
            dataType: 'json',
            url: 'uploads/enviar_arquivo.php',
            resetForm: false
        } ).submit();
    }

    function SalvarFaq() {
        var SupCategoriaId = $( ".SupCategoriaId" ).val();
        var CategoriaId = $( "#CategoriaId" ).val();
        var Pergunta = $( "#NovaPergunta" ).val();
        var Resposta = $( ".NovaResposta" ).val();
        var Link = $( ".NomeArquivo" ).val();


        if ( CategoriaId === "" ) {
            alert( "Informe o ID da Categoria" );
        } else if ( Pergunta === "" ) {
            alert( "Informe a Pergunta" );
        } else if ( Resposta === "" ) {
            alert( "Informe a Resposta ou Selecione um Arquivo" );
        } else if ( Resposta === "Clique aqui para ver a Resposta!" ) {

            if ( Link === "" ) {
                alert( "Selecione um Arquivo" );
            } else {
                AddFaq( SupCategoriaId, CategoriaId, Pergunta, Resposta, Link );
            }
        } else {
            AddFaq( SupCategoriaId, CategoriaId, Pergunta, Resposta, Link );
        }

        function AddFaq( SupCategoriaId, CategoriaId, Pergunta, Resposta, Link ) {

            $.post( "php/php.php", {
                SupCategoriaId: SupCategoriaId,
                CategoriaId: CategoriaId,
                Pergunta: Pergunta,
                Resposta: Resposta,
                Link: Link,
                acao: 'AddFaq'
            }, function( eAddFaq ) {
                if ( eAddFaq ) {

                    alert( 'Faq Cadastrado com Sucesso. \n Aguardando Aprova��o para ser Publicado!' );

                    window.setTimeout( 'location.reload()' );
                }
            } );
        }
    }

    $( "#dialog-form-adicionarFaq" ).dialog( {
        autoOpen: false,
        height: 'auto',
        width: 900,
        modal: true,
        position: ["center", 10],
        buttons: {
            "Salvar": function() {

                var arquivo = $( '.arquivo' ).val();

                if ( arquivo !== "" ) {

                    $.post( "uploads/verificar_arquivo.php", {
                        arquivo: arquivo
                    }, function( eArquivo ) {

                        if ( eArquivo === '1' ) {

                            Upload();

                        } else {
                            $( '.resposta' ).html( ' A Exten��o ' + eArquivo + ' N�o Permitida.' );
                        }
                    } );
                } else {
                    SalvarFaq();
                }
            },
            "Fechar": function() {

                var CategoriaId = $( "#CategoriaId" ).val();
                var Link = $( ".NomeArquivo" ).val();
                if ( Link !== "" ) {

                    $.post( "php/php.php", {
                        CategoriaId: CategoriaId,
                        Link: Link,
                        acao: "DelArquivo"
                    }, function( eDelArquivo ) {

                        if ( eDelArquivo ) {

                            window.setTimeout( 'location.reload()' );
                        }
                    } );
                }
                $( this ).dialog( "close" );
            }
        }
    } );

    //AO CLICAR NO BOT�O DE PESQUISA
    $( ".SearchBtn" ).bind( "click", function() {
        //RECUPERA O VALOR DO CAMPO PARA PESQUISA
        var Search = $( ".Search" ).val();

        //CHAMA FUN��O AJAX
        $.ajax( {
            type: "POST",
            url: "php/php.php",
            data: {
                Search: Search,
                acao: "Search"
            },
            beforeSend: function() {

            },
            complete: function() {

            },
            success: function( eSearch ) {
                //RETORNA O TITULO DA PAGINA DE VISUALIZA��O DE PESQUISA, MOSTRADO A INFORMA��O DIGITADA
                $( '#dialog-form-Searh' ).dialog( 'option', 'title', 'Localizar por: ' + Search );
                //RETORNA O QUE FOI ENCONTRADO E MOSTRA NA TABELA
                $( "#dialog-form-Searh table" ).html( eSearch );
                //CHAMA O DIALOG JQUERY
                $( "#dialog-form-Searh" ).dialog( "open" );
            }
        } );
    } );

    //DIALOG JQUERY COM O RESULTADO DE PESQUISA
    $( "#dialog-form-Searh" ).dialog( {
        autoOpen: false,
        height: 500,
        width: 900,
        modal: true,
        buttons: {
            "Fechar": function() {
                $( this ).dialog( "close" );
            }
        }
    } );

    //AO CLICAR NO LINK PARA ALTERAR A SENHA
    $( ".alterarSenha" ).click( function() {
        //ABRE O DIALOG JQUERY COM O FORMUL�RIO
        $( "#dialog-form-alterarSenha" ).dialog( "open" );
    } );
    //DIALOG JQUERY COM O FORMUL�RIO PARA ALTERAR A SENHA
    $( "#dialog-form-alterarSenha" ).dialog( {
        autoOpen: false,
        height: 'auto',
        width: 350,
        modal: true,
        buttons: {
            "Salvar": function() {

                var SenhaAtual = $( ".SenhaAtual" ).val();

                //RECUPERA O VALOR DA NOVA SENHA
                var Senha = $( ".NovaSenha" ).val();
                //RECUPERA A CONFIRMA��O DA NOVA SENHA
                var ConfSenha = $( ".ConfirmSenha" ).val();

                //VERIFICA SE FOI INFORMADO A SENHA
                if ( SenhaAtual === "" ) {
                    alert( "Informe a Senha Atual" );
                } else if ( Senha === "" ) {
                    alert( "Informe a Nova Senha" );
                    //VERIFICA SE FOI INFORMADO A NOVA SENHA
                } else if ( ConfSenha === "" ) {
                    alert( "Confirme a Nova Senha" );

                    //VERIFICA SE A NOVA SENHA CONFERE COM A CONFIRMA��O DE SENHA
                } else if ( Senha !== ConfSenha ) {
                    alert( "As Senha N�o est�o Iguais" );
                } else {

                    //CHAMA ENVIA OS DADOS AO PHP
                    $.post( "php/php.php", {
                        SenhaAtual: SenhaAtual,
                        Senha: Senha,
                        acao: "AlterarSenha"
                    }, function( eSenha ) {

                        if ( eSenha === '1' ) {
                            alert( "Senha Alterada Com Sucesso!" );
                            //SE TUDO OK FECHA O DIALOG JQUERY
                            window.setTimeout( 'location.reload()' );
                        } else {
                            alert( "Senha Atual Incorreta" );
                        }
                    } );
                }
            },
            "Fechar": function() {
                $( this ).dialog( "close" );
            }
        }
    } );

    //AO CLICAR DA QUEST�O PARA MOSTRAR A RESPOSTA
    $( "body" ).delegate( ".Questao", "click", function() {

        //MOSTRA A LINHA DA TABELA
        $( '.Resposta' ).slideUp( 0.5 );
        //ESCONDE A RESPOSTA DA PERGUNTA QUE N�O FOI CLICADA
        $( this ).parent().parent().next( 'tr' ).slideToggle();
    } );

    //AO CLICAR NA CATEGORIA
    $( ".visualiz" ).click( function() {
        //RECUPERA O ID DA CATEGORIA QUE ESTA NO ATRIBUTO ALT DO LINK
        var idCategoria = $( this ).attr( 'alt' );

        //ENVIA OS DADOS PARA O PHP PARA RECUPERAR O NOME DA CATEGORIA
        $.post( "php/php.php", {
            idCategoria: idCategoria,
            acao: 'ListarTitulo'
        }, function( eListarTitulo ) {
            //RETORNA O NOME DA CATEGORIA
            $( '#dialog-form' ).dialog( 'option', 'title', eListarTitulo );
        } );

        //ENVIA O ID DA CATEGORIA PARA BUSCAR TODOS OS FAQS PERMITIDOS
        $.post( "php/php.php", {
            idCategoria: idCategoria,
            acao: 'ListarQuestoes'
        }, function( eListarQuestoes ) {
            //RETORNA OS VALORES E PREENCHE A TABELA
            $( "#dialog-form table" ).html( eListarQuestoes );
        } );

        //ABRE O DIALOG JQUERY COM AS INFORMA��ES
        $( "#dialog-form" ).dialog( "open" );
    } );

    //DIALOG JQUERY COM OS FAQS DA CATEGORIA
    $( "#dialog-form" ).dialog( {
        autoOpen: false,
        height: 500,
        width: 900,
        modal: true,
        buttons: {
            "Fechar": function() {
                $( this ).dialog( "close" );
            }
        }
    } );
} );