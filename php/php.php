<?php

header( 'Content-Type: text/html; charset=iso 8859-9' );
session_start();
include '../Connections/DB.class.php';
include '../classes/getSet.class.php';
include '../classes/functions.class.php';

$Functions = new functions();

switch ( $_POST['acao'] ) {
    case"DelArquivo":

        if ( unlink( '../admin/uploads/uploads/' . $_POST['CategoriaId'] . '/' . $_POST['Link'] ) ):
            echo 1;
        endif;
        break;
    case"AddFaq":
        //RECEBE O ID DA CATEGORIA
        $Functions->setCategory_id( $_POST['CategoriaId'] );
        //RECEBE O QUE FOI DIGITADO NA PERGUNTA
        $Functions->setQuestion( utf8_decode( $_POST['Pergunta'] ) );
        //RECEBE O QUE FOI DIGITADO NA RESPOSTA
        $Functions->setAnswer( utf8_decode( $_POST['Resposta'] ) );
        //RECEBE O NOME DO ARQUIVO QUE FOI UPDADO
        $Functions->setLink( $_POST['Link'] );
        //RECEBE O ID DO USER LOGADO
        $Functions->setUserInclusao( $_SESSION['MM_UserId'] );
        //CRIA O LINK EXTERNO COM MD5
        $Functions->setLinkExterno( md5( $_POST['CategoriaId'] . '' . $_POST['Pergunta'] . '' . $_POST['Resposta'] ) );

        $Functions->setSupCategoria_id( $_POST['SupCategoriaId'] );
        //CHAMA A FUN��O PARA CADASTRAR E VERIFICA SE FOI CADASTRADO
        if ( $Functions->AddFaq() ):

            //SE FOI CADASTRADO SELECIONA O ID
            //RECUPERANDO PELO ULTIMO ID CADASTRADO
            $Functions->SelectUltFaq();
            //RETORNA O ARRAY
            foreach ( $Functions->SelectUltFaq() as $UltFaq ) {

                $Functions->setCategoria_id( $_POST['CategoriaId'] );
                $Functions->setSupCat_id( $_POST['SupCategoriaId'] );
                $Functions->setFaqs_id( $UltFaq['UltFaq'] );
                //RECEBE O ID DO USU�RIO 1 QUE � ADMINISTRADOR
                $Functions->setUser_id( '1' );
                //CHAMA A FUN��O PARA CADASTRAR A PERMISS�O PARA O ADMINISTRADOR
                if ( $Functions->AddPermission() ):
                    //SE FOR CADASTRADO MOSTRA 1 PARA SER TRATADO PELO JQUERY
                    echo 1;
                endif;
            }
        endif;
        break;
    case"Search":
        //LOCALIZA OS FAQS PELO CAMPO DE LOCALIZAR
        //RECUPERA A INFORMA��O PARA PESQUISA
        $Functions->setQuestion( $_POST['Search'] );
        //RECUPERA O ID DO USU�RIO LOGADO
        $Functions->setIdUser( $_SESSION['MM_UserId'] );
        //CHAMA A FUN��O DE BUSCA
        $CountFaqs = count( $Functions->ListarQuetoesBySearch() );
        //RETORNA O ARRAY
        if ( $CountFaqs <= 0 ):

            echo '<tr class="answer Resposta">
                        <td style="font-size:13px;">Sua Pesquisa n�o gerou resultados.</td>
                </tr>';
        else:
            foreach ( $Functions->ListarQuetoesBySearch() as $Categorias ) {

                $url = explode( "/", $_SERVER['REQUEST_URI'] );
                //VERIFICA O TIPO DE RESPOSTA
                if ( $Categorias['answer'] == 'Clique aqui para ver a Resposta!' ):
                    //LINK PARA ABRIR O ARQUIVO .PDF
                    $Link = '<a href="admin/uploads/uploads/' . $Categorias['category_id'] . '/' . $Categorias['link'] . '" target="_blank" class=".pdf">Clique aqui para ver a Resposta!</a>';
                else:
                    //MOSTRA A RESPOSTA QUANDO � UM TEXTO
                    $Link = $Categorias['answer'];
                endif;

                echo '<tr class="question">
                        <td width="5%"><h1>Q</h1></td>
                        <td width="85%"><a href="javascript:void(0);" class="question Questao">' . $Categorias['id'] . '. ' . $Categorias['question'] . '</a></td>
                        <td style="display: none;">' . $Categorias['category_id'] . '</td>
                </tr>
                <tr class="answer Resposta" style="display:none;">
                        <td><h1>R</h1></td>
                        <td style="font-size:13px;">' . $Link . '</td>
                </tr>
                <tr class="answer ExternoLink" style="display:none;">
                        <td><h1>L</h1></td>
                        <td style="font-size:13px;">http://' . $_SERVER['HTTP_HOST'] . '/' . $url[1] . '/externo.php?link=' . $Categorias['linkExterno'] . '</td>
                </tr>';
            }
        endif;
        break;
    case"AlterarSenha":
        //ALTERAR SENHA DO USU�RIO / TODOS OS USU�RIOS PODEM TROCAR SUA PR�RPIA SENHA
        $Functions->setPasswrd( md5( $_POST['SenhaAtual'] ) );
        //RECUPERA O ID DO USUARIO LOGADO
        $Functions->setIdUser( $_SESSION['MM_UserId'] );

        $CountVerificarSenha = count( $Functions->VerificarSenhaAtual() );

        if ( $CountVerificarSenha == 0 ):
            echo 0;
        else:
            //RECUPERA A NOVA SENHA E CLIPTOGRAFA EM MD5
            $Functions->setPasswrd( md5( $_POST['Senha'] ) );
            //CHAMA A FUN��O PARA ALTERAR A SENHA
            if ( $Functions->AlterarSenha() ):
                //SE TU OK MOSTRA 1 PARA TRATAMENTO DE RESPOSTA COM JAVASCRIPT
                echo 1;
            endif;
        endif;


        break;
    case"ListarQuestoes":
        //LISTA OS FAQS NO MOMENTO QUE SE CLICA NA CATEGORIA PARA VISUALIZAR TODOS OS FAQS PERMITIDOS
        //PARA UMA DETERMINADA CATEGORIA
        //RECUPERA O ID DA CATEGORIA
        $Functions->setCategory_id( $_POST['idCategoria'] );
        //RECUPERA O ID DO USUARIO LOGADO
        $Functions->setIdUser( $_SESSION['MM_UserId'] );
        //CHAMA A FUN��O
        $Functions->ListarQuetoesIdCategoria();

        foreach ( $Functions->ListarQuetoesIdCategoria() as $Categorias ) {
            //VERIFICA O TIPO DE RESPOSTA
            if ( $Categorias['answer'] == 'Clique aqui para ver a Resposta!' ):
                //SE TIVER A RESPOSTA 'Clique aqui para ver a Resposta!' VAI APARECER O LINK PARA O PDF
                $Link = '<a href="admin/uploads/uploads/' . $Categorias['category_id'] . '/' . $Categorias['link'] . '" target="_blank" class=".pdf">Clique aqui para ver a Resposta!</a>';
            else:
                //SE FOR OUTRO TIPO DE RESPOSTA, MOSTRA A RESPOSTA NORMAL
                $Link = $Categorias['answer'];
            endif;

            echo '<tr class="question">
                        <td width="5%"><h1>Q</h1></td>
                        <td width="95%"><a href="javascript:void(0);" class="question Questao">' . $Categorias['id'] . '. ' . $Categorias['question'] . '</a></td>
                </tr>
                <tr class="answer Resposta" style="display:none;">
                        <td><h1>R</h1></td>
                        <td style="font-size:13px;">' . $Link . '</td>
                </tr>';
        }
        break;
    case"ListarTitulo":
        //LISTA O NOME DA CATEGORIA PARA INFOMAR NO CABE�ALHO DO POPUP(DIALOG JQUERY)
        //RECUPERA O ID DA CATEGORIA
        $Functions->setCategory_id( $_POST['idCategoria'] );
        //CHAMA A FUN��O
        $Functions->ListarTituloId();

        foreach ( $Functions->ListarTituloId() as $Categorias ) {
            //RETORNA O NOME DA CATEGORIA
            echo $Categorias['category'];
        }
        break;
}
?>
